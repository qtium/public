
## Data Amount (min)
|Room|201|203|205|
|:-:|-:|-:|-:|
|Train|127999|162548|335817|
|Test|66900|9234|161882|


## Result
||time_width|201|203|205|201|203|205|
|-|-:|:-:|:-:|:-:|-:|-:|-:|
|Dense|120|o|||89.15|76.49|83.54|
|Dense|120||o||72.43|92.39|70.18|
|Dense|120|||o|62.28|56.10|82.11|
|Shared Conv|120|o|||90.19|77.16|86.10|
|Shared Conv|120|||o|||87.37|
|Shared Conv|120|o|o|o|91.71|93.23|89.66|
|Sensor-Wise Conv|120|o|||90.46|77.54|85.51|
|Sensor-Wise Conv|120|o|o|o|92.17|94.87|90.23|
|Sensor-Wise Conv|240|o|o|o|92.08|94.13|90.34|
|Sensor-Wise Conv|720|o|o|o|86.88|93.60|84.90|
|Sensor-Wise Conv Deeper|120|o|o|o|92.74|91.54|90.17|
|Dilated Conv|128|o|o|o|90.94|93.83|90.16|
|Dilated Conv|256|o|o|o|90.42|95.02|88.51|
|LSTM|120|o|o|o|90.87|92.72|90.42|


```python
# Keras
from keras import regularizers
from keras import backend as K
from keras.callbacks import EarlyStopping
from keras.layers import (
    BatchNormalization,
    Concatenate,
    Conv1D,
    Conv2D,
    Cropping1D,
    Dense,
    Dropout,
    Embedding,
    Flatten,
    Input,
    Lambda,
    LeakyReLU,
    LSTM,
    MaxPooling1D,
    MaxPooling2D,
    Reshape,
)
from keras.models import Model
from keras.optimizers import Adadelta, Adam, RMSprop, Adagrad, SGD
from keras.utils import (
    Sequence,
    to_categorical,
)

# Others
import homeiot
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import pdutil
import seaborn as sns
from sklearn.manifold import TSNE
from sklearn.metrics import confusion_matrix
from sklearn.preprocessing import LabelEncoder, LabelBinarizer
from tqdm import tqdm

# Local
from qrnn import QRNN

def init():
    homeiot.Visualizer()

init()
```

    /home/eguchi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/h5py/__init__.py:36: FutureWarning: Conversion of the second argument of issubdtype from `float` to `np.floating` is deprecated. In future, it will be treated as `np.float64 == np.dtype(float).type`.
      from ._conv import register_converters as _register_converters
    Using TensorFlow backend.
    


```python
lb = LabelBinarizer().fit(['In-room', 'Go-out', 'Sleep'])
lb.classes_
```




    array(['Go-out', 'In-room', 'Sleep'], dtype='<U7')




```python
def get_lpp_data(index):
    if not hasattr(get_lpp_data, 'sn'):
        get_lpp_data.sn = homeiot.SensorNet(kind='LPP')
    lpp = get_lpp_data.sn.load_df(index[0]-60, index[-1]+60)
    return lpp.slice().getX(index=index)
```


```python
def get_month(index):
    return index.month - 1
```


```python
def get_dayofweek(index):
    return index.dayofweek
```


```python
def get_day(index):
    return index.day - 1
```


```python
def get_clock(index):
    return homeiot.Preprocessor.clock_time(index)
```


```python
def get_data(pkl, funcs=[]):
    """
    {'201': [
        [[span],...],
        [[func0_out],...],
        ...
        ],
     ...
    }
    """
    X, Y, D = {}, {}, {}
    for room in pkl:
        room_num, data = room['room_num'], room['data']
        sn = homeiot.SensorNet(room=room_num).drop(place='window')
        xaxa, yy, dd = [], [], []
        for df in tqdm(data):
            index = df.index
            label = df.sleep_tag
            sn.load_df(index[0]-60, index[-1]+60)
            x = sn.slice().getX(index=index)
            y = lb.transform(label)
            assert x.shape[0] == y.shape[0]
            xa = []
            xa.append(x)
            for func in funcs:
                xa.append(func(index))
            xaxa.append(xa)
            yy.append(y)
            dd.append(index)            
        X[room_num] = xaxa
        Y[room_num] = yy
        D[room_num] = dd
    return X, Y, D
```


```python
class RoomSequence(Sequence):
    def __init__(self, X, Y, D, time_width, batch_size=1):
        self.X, self.Y, self.D = X, Y, D
        self.time_width = time_width
        self.batch_size = batch_size
        
        self.exclude_nan()
        self.merge_chunks()
        
    def exclude_nan(self):
        index = [not any([np.isnan(x).any() for x in xa]) for xa in self.X]
        self.X = [self.X[i] for i in np.where(index)[0]]
        self.Y = np.array(self.Y)[index]
        self.D = np.array(self.D)[index]

    def set_time_width(self, time_width):
        self.time_width = time_width
        self.merge_chunks()
    
    def merge_chunks(self):
        bool_index = []
        for y in self.Y:
            index = np.ones(y.shape[0], dtype=bool)
            if time_width > 2:
                index[-self.time_width+1:] = False# TODO: The left margin of X should be included.
            bool_index.append(index)
        bool_index = np.concatenate(bool_index, axis=0)
        self.MI = np.arange(len(bool_index))[bool_index]
        self.MX = []
        for x in [[_[i] for _ in self.X] for i in range(len(self.X[0]))]:
            self.MX.append(np.concatenate(x, axis=0))
        self.MY = np.concatenate(self.Y, axis=0)
        self.MD = np.concatenate(self.D, axis=0)
        
    def __getitem__(self, idx):
        index = self.MI[self.batch_size*idx:self.batch_size*(idx+1)]
        return (
            [np.array([xx[head:head+self.time_width] for head in index]) for xx in self.MX],
            np.array([self.MY[head+self.time_width-1] for head in index])
        )
    
    def __len__(self):
        return int(np.ceil(len(self.MI) / self.batch_size))
    
    def __add__(self, other):
        if other is None:
            return self
        X_ = list(self.X)
        X_.extend(other.X)
        Y_ = list(self.Y)
        Y_.extend(other.Y)
        D_ = list(self.D)
        D_.extend(other.D)
        return self.__class__(X_, Y_, D_, self.time_width, self.batch_size)

    def __radd__(self, other):
        return self
```


```python
def test_val_split(X, Y, D, room_nums, time_width, batch_size):
    """ Returns RoomSequence instance.
    """
    if type(room_nums) != list:
        room_nums = [room_nums]
    train_seq, val_seq = None, None
    for room_num in room_nums:
        xaxa = X[room_num]
        yy = Y[room_num]
        dd = D[room_num]
        train_seq += RoomSequence(xaxa[:-3], yy[:-3], dd[:-3], time_width=time_width, batch_size=batch_size)
        val_seq += RoomSequence(xaxa[-3:], yy[-3:], dd[-3:], time_width=time_width, batch_size=batch_size)
    return train_seq, val_seq
```


```python
# def generator(X, Y, time_width, room_nums=[], val_idx=[]):
#     def _gen(val):
#         while True:
#             for room_num in room_nums:
#                 room_X = np.array(X[room_num]).T
#                 room_Y = np.array(Y[room_num])
#                 assert room_X.shape[0] == room_Y.shape[0]
#                 if val:
#                     key = np.zeros(room_X.shape[0], dtype=bool)
#                 else:
#                     key = np.ones(room_X.shape[0], dtype=bool)
#                 key[val_idx] = ~key[val_idx]
#                 for key_, (row, yy) in enumerate(zip(room_X[key], room_Y[key])):
#                     if any([np.isnan(xx).any() for xx in row]):
#                         print('Room {} key {} has nan value.'.format(room_num, key_))
#                         continue
#                     for i in range(time_width, len(yy)+1):
#                         xxx = [np.array(xx[i-time_width:i]).reshape(1, time_width, -1) for xx in row]
#                         yield (
#                             xxx, 
#                             yy[time_width-1].reshape(1, -1)
#                         )
#     return _gen(False), _gen(True)
```


```python
try:
    with open('train_data.pkl', 'rb') as f:
        train_X, train_Y, train_D = pickle.load(f)
except FileNotFoundError:
    train_pkl = pd.read_pickle("../E-tei/sensor2vec/LearningData/train_data.pkl")
    train_X, train_Y, train_D = get_data(train_pkl, funcs=[get_lpp_data, get_month, get_dayofweek, get_day, get_clock])
    with open('train_data.pkl', 'wb') as f:
        pickle.dump((train_X, train_Y, train_D), f)
```


```python
try:
    with open('test_data.pkl', 'rb') as f:
        test_X, test_Y, test_D = pickle.load(f)
except FileNotFoundError:
    test_pkl = pd.read_pickle("../E-tei/sensor2vec/LearningData/test_data.pkl")
    test_X, test_Y, test_D = get_data(test_pkl, funcs=[get_lpp_data, get_month, get_dayofweek, get_day, get_clock])
    with open('test_data.pkl', 'wb') as f:
        pickle.dump((test_X, test_Y, test_D), f)
```

# Dense


```python
class SensorNN():
    
    # Dimension of Common Sensor Data
    COMMON_NDIM = 1
    
    # Dimensions of Time Representation
    MAX_MONTH = 12
    MAX_WEEK = 7
    MAX_DAY = 31
    
    # Dimensions of Time Embedding
    EMBED_MONTH = 2
    EMBED_WEEK = 2
    EMBED_DAY = 2
    
    # Regularizer of Time Embedding
    EMBED_REG_MONTH = regularizers.l1(0.001)
    EMBED_REG_WEEK = regularizers.l1(0.001)
    EMBED_REG_DAY = regularizers.l1(0.001)
    
    def __init__(self, time_width, sensor_ndim):
        self.time_width = time_width
        self.sensor_ndim = sensor_ndim
        self.build()
        
    """ Build Functions
    """
    def build(self):
        """ Build whole architecture.
        """
        self.build_input()
        self.build_embedding()
        self.build_core()
        self.model = Model(self._input, self._output)
        return self

    def build_input(self):
        """ Define inputs.
        """
        self._input_sensor = Input(shape=(self.time_width, self.sensor_ndim), name='input_sensor')
        self._input_common = Input(shape=(self.time_width, self.COMMON_NDIM), name='input_common')
        self._input_month = Input(shape=(self.time_width, ), name='input_month')
        self._input_dayofweek = Input(shape=(self.time_width, ), name='input_dayofweek')
        self._input_day = Input(shape=(self.time_width, ), name='input_day')
        self._input_clock = Input(shape=(self.time_width, 2), name='input_clock')
        self._input = [self._input_sensor,
                       self._input_common,
                       self._input_month,
                       self._input_dayofweek,
                       self._input_day,
                       self._input_clock,
                      ]

    def build_embedding(self):
        """ Define embedding layers.
        """
        self.embedding_month = Embedding(self.MAX_MONTH, self.EMBED_MONTH, embeddings_regularizer=self.EMBED_REG_MONTH, name='embedding_month')
        self.embedding_dayofweek = Embedding(self.MAX_WEEK, self.EMBED_WEEK, embeddings_regularizer=self.EMBED_REG_WEEK, name='embedding_dayofweek')
        self.embedding_day = Embedding(self.MAX_DAY, self.EMBED_DAY, embeddings_regularizer=self.EMBED_REG_DAY, name='embedding_day')
        
        self._embedding_month = self.embedding_month(self._input_month)
        self._embedding_dayofweek = self.embedding_dayofweek(self._input_dayofweek)
        self._embedding_day = self.embedding_day(self._input_day)

        self.embedding = [self._input_sensor,
                          self._input_common,
                          self._embedding_month,
                          self._embedding_dayofweek,
                          self._embedding_day,
                          self._input_clock,
                         ]

    def build_core(self):
        """ Define core architecture.
        """
        # Concatenate Input and Embeddings #
        _merged = Concatenate(axis=2)(self.embedding)
        
        # Dense #
        flatten = Flatten()
        dense0 = Dense(16)
        lrelu = LeakyReLU()
        dense1 = Dense(3, activation='softmax')

        _flatten = flatten(_merged)
        _dense0 = dense0(_flatten)
        _dense1 = dense1(lrelu(_dense0))
        self._output = _dense1

    """ Keras Model Functions
    """
    def compile(self, **kwargs):
        kwargs['metrics'] = ['accuracy']
        self.model.compile(**kwargs)
        return self
    
    def fit_generator(self, *args, **kwargs):
        early_stopping = EarlyStopping(monitor='val_loss', patience=6)
#         kwargs['steps_per_epoch'] = 60
        kwargs['max_queue_size'] = 1024
        kwargs['epochs'] = 500
        kwargs['callbacks'] = [early_stopping]
        self.history = self.model.fit_generator(*args, **kwargs)
        self.plot_history()
        
    def plot_history(self):
        homeiot.Visualizer()
        plt.figure()
        plt.plot(self.history.history['loss'], label="loss")
        plt.plot(self.history.history['val_loss'], label="val_loss")
        plt.legend()
```

#### Room 201


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '201', time_width, 1024)
nn = SensorNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_6 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    flatten_6 (Flatten)             (None, 2880)         0           concatenate_6[0][0]              
    __________________________________________________________________________________________________
    dense_11 (Dense)                (None, 16)           46096       flatten_6[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 16)           0           dense_11[0][0]                   
    __________________________________________________________________________________________________
    dense_12 (Dense)                (None, 3)            51          leaky_re_lu_6[0][0]              
    ==================================================================================================
    Total params: 46,247
    Trainable params: 46,247
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    103/103 [==============================] - 1s 14ms/step - loss: 0.6208 - acc: 0.7887 - val_loss: 0.7105 - val_acc: 0.7264
    Epoch 2/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.4326 - acc: 0.8594 - val_loss: 0.6328 - val_acc: 0.7617
    Epoch 3/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.3946 - acc: 0.8725 - val_loss: 0.5612 - val_acc: 0.7933
    Epoch 4/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.3683 - acc: 0.8812 - val_loss: 0.4898 - val_acc: 0.8337
    Epoch 5/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.3479 - acc: 0.8902 - val_loss: 0.4513 - val_acc: 0.8700
    Epoch 6/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.3345 - acc: 0.8992 - val_loss: 0.4284 - val_acc: 0.8997
    Epoch 7/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.3194 - acc: 0.9039 - val_loss: 0.4130 - val_acc: 0.9077
    Epoch 8/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.3087 - acc: 0.9060 - val_loss: 0.4057 - val_acc: 0.9109
    Epoch 9/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.2984 - acc: 0.9099 - val_loss: 0.4042 - val_acc: 0.9124
    Epoch 10/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.2879 - acc: 0.9096 - val_loss: 0.3914 - val_acc: 0.9136
    Epoch 11/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.2785 - acc: 0.9115 - val_loss: 0.3886 - val_acc: 0.9138
    Epoch 12/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.2676 - acc: 0.9131 - val_loss: 0.3938 - val_acc: 0.9106
    Epoch 13/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.2582 - acc: 0.9136 - val_loss: 0.3907 - val_acc: 0.9121
    Epoch 14/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.2502 - acc: 0.9139 - val_loss: 0.3998 - val_acc: 0.9136
    Epoch 15/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.2412 - acc: 0.9124 - val_loss: 0.4054 - val_acc: 0.9141
    Epoch 16/500
    103/103 [==============================] - 1s 12ms/step - loss: 0.2361 - acc: 0.9134 - val_loss: 0.4160 - val_acc: 0.9096
    Epoch 17/500
    103/103 [==============================] - 1s 11ms/step - loss: 0.2313 - acc: 0.9123 - val_loss: 0.4229 - val_acc: 0.9097
    


![png](output_17_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.36237392563125403, 0.8915275503236961]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [1.4402618563790837, 0.7649061028946174]




```python
nn.model.evaluate_generator(RoomSequence(train_X['203'], train_Y['203'], train_D['203'], time_width=time_width, batch_size=1024))
```




    [1.6987347320556785, 0.7187018373965512]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.5296876030438133, 0.8354415845407868]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.7934891638071554, 0.7583083547865576]



#### Room 203


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '203', time_width=time_width, batch_size=1024)
nn = SensorNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_5 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    flatten_5 (Flatten)             (None, 2880)         0           concatenate_5[0][0]              
    __________________________________________________________________________________________________
    dense_9 (Dense)                 (None, 16)           46096       flatten_5[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 16)           0           dense_9[0][0]                    
    __________________________________________________________________________________________________
    dense_10 (Dense)                (None, 3)            51          leaky_re_lu_5[0][0]              
    ==================================================================================================
    Total params: 46,247
    Trainable params: 46,247
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    125/125 [==============================] - 2s 14ms/step - loss: 0.6857 - acc: 0.7532 - val_loss: 0.5567 - val_acc: 0.8171
    Epoch 2/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.4777 - acc: 0.8367 - val_loss: 0.4992 - val_acc: 0.8369
    Epoch 3/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.4253 - acc: 0.8529 - val_loss: 0.4676 - val_acc: 0.8482
    Epoch 4/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.3935 - acc: 0.8643 - val_loss: 0.4481 - val_acc: 0.8558
    Epoch 5/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.3746 - acc: 0.8693 - val_loss: 0.4366 - val_acc: 0.8653
    Epoch 6/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.3579 - acc: 0.8778 - val_loss: 0.4161 - val_acc: 0.8648
    Epoch 7/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.3465 - acc: 0.8796 - val_loss: 0.4148 - val_acc: 0.8678
    Epoch 8/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.3347 - acc: 0.8822 - val_loss: 0.4088 - val_acc: 0.8727
    Epoch 9/500
    125/125 [==============================] - 1s 12ms/step - loss: 0.3253 - acc: 0.8882 - val_loss: 0.4038 - val_acc: 0.8769
    Epoch 10/500
    125/125 [==============================] - 2s 12ms/step - loss: 0.3106 - acc: 0.8927 - val_loss: 0.3871 - val_acc: 0.8797
    Epoch 11/500
    125/125 [==============================] - 2s 12ms/step - loss: 0.3028 - acc: 0.8950 - val_loss: 0.3959 - val_acc: 0.8820
    Epoch 12/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.2945 - acc: 0.8974 - val_loss: 0.3992 - val_acc: 0.8771
    Epoch 13/500
    125/125 [==============================] - 1s 12ms/step - loss: 0.2874 - acc: 0.8981 - val_loss: 0.3958 - val_acc: 0.8784
    Epoch 14/500
    125/125 [==============================] - 2s 12ms/step - loss: 0.2797 - acc: 0.9010 - val_loss: 0.3952 - val_acc: 0.8796
    Epoch 15/500
    125/125 [==============================] - 2s 12ms/step - loss: 0.2693 - acc: 0.9038 - val_loss: 0.3941 - val_acc: 0.8780
    Epoch 16/500
    125/125 [==============================] - 2s 13ms/step - loss: 0.2626 - acc: 0.9048 - val_loss: 0.3991 - val_acc: 0.8783
    


![png](output_24_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.6908423363473398, 0.7243125795289271]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.289065081524737, 0.9239436629232667]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.7022709847507957, 0.7017829234817247]



#### Room 205


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '205', time_width, 1024)
nn = SensorNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_3 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    flatten_3 (Flatten)             (None, 2880)         0           concatenate_3[0][0]              
    __________________________________________________________________________________________________
    dense_5 (Dense)                 (None, 16)           46096       flatten_3[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 16)           0           dense_5[0][0]                    
    __________________________________________________________________________________________________
    dense_6 (Dense)                 (None, 3)            51          leaky_re_lu_3[0][0]              
    ==================================================================================================
    Total params: 46,247
    Trainable params: 46,247
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    296/296 [==============================] - 3s 10ms/step - loss: 0.5193 - acc: 0.8143 - val_loss: 0.7269 - val_acc: 0.7054
    Epoch 2/500
    296/296 [==============================] - 3s 9ms/step - loss: 0.4040 - acc: 0.8591 - val_loss: 0.7616 - val_acc: 0.7167
    Epoch 3/500
    296/296 [==============================] - 3s 10ms/step - loss: 0.3739 - acc: 0.8693 - val_loss: 0.7093 - val_acc: 0.7188
    Epoch 4/500
    296/296 [==============================] - 3s 10ms/step - loss: 0.3526 - acc: 0.8751 - val_loss: 0.7199 - val_acc: 0.6959
    Epoch 5/500
    296/296 [==============================] - 3s 10ms/step - loss: 0.3431 - acc: 0.8774 - val_loss: 0.7341 - val_acc: 0.7293
    Epoch 6/500
    296/296 [==============================] - 3s 9ms/step - loss: 0.3289 - acc: 0.8811 - val_loss: 0.7413 - val_acc: 0.7180
    Epoch 7/500
    296/296 [==============================] - 3s 9ms/step - loss: 0.3134 - acc: 0.8839 - val_loss: 0.7460 - val_acc: 0.7106
    Epoch 8/500
    296/296 [==============================] - 3s 9ms/step - loss: 0.3049 - acc: 0.8866 - val_loss: 0.7988 - val_acc: 0.7146
    Epoch 9/500
    296/296 [==============================] - 3s 10ms/step - loss: 0.2976 - acc: 0.8879 - val_loss: 0.7815 - val_acc: 0.6979
    


![png](output_29_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.7250452692043775, 0.6227870182035354]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.832075813286741, 0.5610328632341304]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.37935979639354994, 0.8211016489490423]



# Normal CNN


```python
class SensorCNN(SensorNN):
    def build_core(self):

        _merged = Concatenate(axis=2)(self.embedding)
        
        shape = K.int_shape(_merged)[1:] + (1, )
        _image = Reshape(shape)(_merged)
        
        # Conv 1 #
        _conv = _image

        conv = Conv2D(16, (3, 1), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        conv = Conv2D(16, (1, 24), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)
        
        mpool = MaxPooling2D((3, 1), padding='valid')
        _mpool = mpool(_conv)

        # Conv 2 #
        _conv = _mpool

        conv = Conv2D(32, (3, 1), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        conv = Conv2D(32, (1, 24), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling2D((2, 1), padding='valid')
        _mpool = mpool(_conv)

        # Conv 3 #
        _conv = _mpool

        conv = Conv2D(32, (3, 1), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        conv = Conv2D(64, (1, 24), padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling2D((2, 24), padding='valid')
        _mpool = mpool(_conv)
        
        
        # Dense #
        flatten = Flatten()
        _flatten = flatten(_mpool)
        
        dense = Dense(16)
        _dense = dense(_flatten)
        _dense = LeakyReLU()(_dense)
        
        dense = Dense(3, activation='softmax')
        _dense = dense(_dense)
        
        self._output = _dense
```


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '201', time_width, 1024)
nn = SensorCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_10 (Concatenate)    (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    reshape_4 (Reshape)             (None, 120, 24, 1)   0           concatenate_10[0][0]             
    __________________________________________________________________________________________________
    conv2d_19 (Conv2D)              (None, 120, 24, 16)  64          reshape_4[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_28 (LeakyReLU)      (None, 120, 24, 16)  0           conv2d_19[0][0]                  
    __________________________________________________________________________________________________
    conv2d_20 (Conv2D)              (None, 120, 24, 16)  6160        leaky_re_lu_28[0][0]             
    __________________________________________________________________________________________________
    leaky_re_lu_29 (LeakyReLU)      (None, 120, 24, 16)  0           conv2d_20[0][0]                  
    __________________________________________________________________________________________________
    max_pooling2d_10 (MaxPooling2D) (None, 40, 24, 16)   0           leaky_re_lu_29[0][0]             
    __________________________________________________________________________________________________
    conv2d_21 (Conv2D)              (None, 40, 24, 32)   1568        max_pooling2d_10[0][0]           
    __________________________________________________________________________________________________
    leaky_re_lu_30 (LeakyReLU)      (None, 40, 24, 32)   0           conv2d_21[0][0]                  
    __________________________________________________________________________________________________
    conv2d_22 (Conv2D)              (None, 40, 24, 32)   24608       leaky_re_lu_30[0][0]             
    __________________________________________________________________________________________________
    leaky_re_lu_31 (LeakyReLU)      (None, 40, 24, 32)   0           conv2d_22[0][0]                  
    __________________________________________________________________________________________________
    max_pooling2d_11 (MaxPooling2D) (None, 20, 24, 32)   0           leaky_re_lu_31[0][0]             
    __________________________________________________________________________________________________
    conv2d_23 (Conv2D)              (None, 20, 24, 32)   3104        max_pooling2d_11[0][0]           
    __________________________________________________________________________________________________
    leaky_re_lu_32 (LeakyReLU)      (None, 20, 24, 32)   0           conv2d_23[0][0]                  
    __________________________________________________________________________________________________
    conv2d_24 (Conv2D)              (None, 20, 24, 64)   49216       leaky_re_lu_32[0][0]             
    __________________________________________________________________________________________________
    leaky_re_lu_33 (LeakyReLU)      (None, 20, 24, 64)   0           conv2d_24[0][0]                  
    __________________________________________________________________________________________________
    max_pooling2d_12 (MaxPooling2D) (None, 10, 1, 64)    0           leaky_re_lu_33[0][0]             
    __________________________________________________________________________________________________
    flatten_10 (Flatten)            (None, 640)          0           max_pooling2d_12[0][0]           
    __________________________________________________________________________________________________
    dense_19 (Dense)                (None, 16)           10256       flatten_10[0][0]                 
    __________________________________________________________________________________________________
    leaky_re_lu_34 (LeakyReLU)      (None, 16)           0           dense_19[0][0]                   
    __________________________________________________________________________________________________
    dense_20 (Dense)                (None, 3)            51          leaky_re_lu_34[0][0]             
    ==================================================================================================
    Total params: 95,127
    Trainable params: 95,127
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    103/103 [==============================] - 22s 212ms/step - loss: 0.9353 - acc: 0.6167 - val_loss: 0.6964 - val_acc: 0.8224
    Epoch 2/500
    103/103 [==============================] - 21s 203ms/step - loss: 0.5407 - acc: 0.8277 - val_loss: 0.5580 - val_acc: 0.7505
    Epoch 3/500
    103/103 [==============================] - 21s 203ms/step - loss: 0.4082 - acc: 0.8720 - val_loss: 0.4990 - val_acc: 0.7732
    Epoch 4/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.3918 - acc: 0.8765 - val_loss: 0.4695 - val_acc: 0.8051
    Epoch 5/500
    103/103 [==============================] - 21s 205ms/step - loss: 0.3660 - acc: 0.8869 - val_loss: 0.4566 - val_acc: 0.8438
    Epoch 6/500
    103/103 [==============================] - 21s 205ms/step - loss: 0.3414 - acc: 0.9006 - val_loss: 0.3883 - val_acc: 0.9234
    Epoch 7/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.3380 - acc: 0.9044 - val_loss: 0.3552 - val_acc: 0.9222
    Epoch 8/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.3191 - acc: 0.9068 - val_loss: 0.3407 - val_acc: 0.9136
    Epoch 9/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2969 - acc: 0.9117 - val_loss: 0.3595 - val_acc: 0.9122
    Epoch 10/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2994 - acc: 0.9095 - val_loss: 0.3443 - val_acc: 0.9245
    Epoch 11/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2891 - acc: 0.9142 - val_loss: 0.3320 - val_acc: 0.9198
    Epoch 12/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2801 - acc: 0.9148 - val_loss: 0.3200 - val_acc: 0.9237
    Epoch 13/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2860 - acc: 0.9109 - val_loss: 0.3203 - val_acc: 0.9259
    Epoch 14/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2795 - acc: 0.9127 - val_loss: 0.2973 - val_acc: 0.9259
    Epoch 15/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2741 - acc: 0.9150 - val_loss: 0.3730 - val_acc: 0.9020
    Epoch 16/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2640 - acc: 0.9148 - val_loss: 0.3008 - val_acc: 0.9254
    Epoch 17/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2627 - acc: 0.9178 - val_loss: 0.2895 - val_acc: 0.9228
    Epoch 18/500
    103/103 [==============================] - 21s 205ms/step - loss: 0.2689 - acc: 0.9143 - val_loss: 0.3064 - val_acc: 0.9249
    Epoch 19/500
    103/103 [==============================] - 21s 205ms/step - loss: 0.2519 - acc: 0.9150 - val_loss: 0.3141 - val_acc: 0.9268
    Epoch 20/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2540 - acc: 0.9158 - val_loss: 0.2935 - val_acc: 0.9270
    Epoch 21/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2431 - acc: 0.9178 - val_loss: 0.3057 - val_acc: 0.9256
    Epoch 22/500
    103/103 [==============================] - 21s 204ms/step - loss: 0.2396 - acc: 0.9176 - val_loss: 0.3224 - val_acc: 0.9262
    Epoch 23/500
    103/103 [==============================] - 21s 205ms/step - loss: 0.2396 - acc: 0.9181 - val_loss: 0.2962 - val_acc: 0.9237
    


![png](output_35_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.2985465847409631, 0.9018866769970489]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.9785155503962522, 0.7715962443553226]




```python
nn.model.evaluate_generator(RoomSequence(train_X['203'], train_Y['203'], train_D['203'], time_width=time_width, batch_size=1024))
```




    [1.097095571676514, 0.7694537290081546]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.4555186544847356, 0.8609830000452025]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.4531984302274122, 0.8462152040315027]




```python
K.clear_session()
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '205', time_width, 1024)
nn = SensorCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    reshape_1 (Reshape)             (None, 120, 24, 1)   0           concatenate_1[0][0]              
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 120, 24, 16)  64          reshape_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_1[0][0]                   
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 120, 24, 16)  6160        leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_1 (MaxPooling2D)  (None, 40, 24, 16)   0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv2d_3 (Conv2D)               (None, 40, 24, 16)   784         max_pooling2d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 40, 24, 16)   0           conv2d_3[0][0]                   
    __________________________________________________________________________________________________
    conv2d_4 (Conv2D)               (None, 40, 24, 64)   24640       leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 40, 24, 64)   0           conv2d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_2 (MaxPooling2D)  (None, 20, 12, 64)   0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv2d_5 (Conv2D)               (None, 20, 12, 16)   3088        max_pooling2d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 20, 12, 16)   0           conv2d_5[0][0]                   
    __________________________________________________________________________________________________
    conv2d_6 (Conv2D)               (None, 20, 12, 16)   6160        leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 20, 12, 16)   0           conv2d_6[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_3 (MaxPooling2D)  (None, 10, 6, 16)    0           leaky_re_lu_6[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 960)          0           max_pooling2d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           15376       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_7 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_7[0][0]              
    ==================================================================================================
    Total params: 56,423
    Trainable params: 56,423
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    60/60 [==============================] - 12s 203ms/step - loss: 1.0396 - acc: 0.4652 - val_loss: 0.9582 - val_acc: 0.4010
    Epoch 2/500
    60/60 [==============================] - 11s 187ms/step - loss: 0.7776 - acc: 0.5918 - val_loss: 0.8027 - val_acc: 0.5140
    Epoch 3/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.6242 - acc: 0.7748 - val_loss: 0.6789 - val_acc: 0.7419
    Epoch 4/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.4845 - acc: 0.8341 - val_loss: 0.6568 - val_acc: 0.7420
    Epoch 5/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.3697 - acc: 0.8874 - val_loss: 0.7227 - val_acc: 0.7449
    Epoch 6/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.4028 - acc: 0.8640 - val_loss: 0.6373 - val_acc: 0.7518
    Epoch 7/500
    60/60 [==============================] - 11s 187ms/step - loss: 0.3745 - acc: 0.8808 - val_loss: 0.6418 - val_acc: 0.7443
    Epoch 8/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.4144 - acc: 0.8459 - val_loss: 0.6310 - val_acc: 0.7533
    Epoch 9/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.4084 - acc: 0.8632 - val_loss: 0.6159 - val_acc: 0.7552
    Epoch 10/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.4464 - acc: 0.8462 - val_loss: 0.6097 - val_acc: 0.7449
    Epoch 11/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.4206 - acc: 0.8573 - val_loss: 0.6177 - val_acc: 0.7476
    Epoch 12/500
    60/60 [==============================] - 11s 190ms/step - loss: 0.4350 - acc: 0.8451 - val_loss: 0.5643 - val_acc: 0.7613
    Epoch 13/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.3301 - acc: 0.8913 - val_loss: 0.6067 - val_acc: 0.7468
    Epoch 14/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.3958 - acc: 0.8575 - val_loss: 0.5722 - val_acc: 0.7627
    Epoch 15/500
    60/60 [==============================] - 11s 189ms/step - loss: 0.3602 - acc: 0.8775 - val_loss: 0.6213 - val_acc: 0.7474
    Epoch 16/500
    60/60 [==============================] - 11s 188ms/step - loss: 0.3421 - acc: 0.8853 - val_loss: 0.5970 - val_acc: 0.7562
    Epoch 17/500
    60/60 [==============================] - 11s 190ms/step - loss: 0.3619 - acc: 0.8736 - val_loss: 0.5733 - val_acc: 0.7615
    Epoch 18/500
    60/60 [==============================] - 11s 190ms/step - loss: 0.4293 - acc: 0.8374 - val_loss: 0.5796 - val_acc: 0.7585
    


![png](output_41_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.35639143717438015, 0.8736771600803751]



### Increase data amount...


```python
K.clear_session()
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    reshape_1 (Reshape)             (None, 120, 24, 1)   0           concatenate_1[0][0]              
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 120, 24, 16)  64          reshape_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_1[0][0]                   
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 120, 24, 16)  6160        leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_1 (MaxPooling2D)  (None, 40, 24, 16)   0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv2d_3 (Conv2D)               (None, 40, 24, 32)   1568        max_pooling2d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 40, 24, 32)   0           conv2d_3[0][0]                   
    __________________________________________________________________________________________________
    conv2d_4 (Conv2D)               (None, 40, 24, 32)   24608       leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 40, 24, 32)   0           conv2d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_2 (MaxPooling2D)  (None, 20, 24, 32)   0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv2d_5 (Conv2D)               (None, 20, 24, 32)   3104        max_pooling2d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 20, 24, 32)   0           conv2d_5[0][0]                   
    __________________________________________________________________________________________________
    conv2d_6 (Conv2D)               (None, 20, 24, 64)   49216       leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 20, 24, 64)   0           conv2d_6[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_3 (MaxPooling2D)  (None, 10, 1, 64)    0           leaky_re_lu_6[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 640)          0           max_pooling2d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           10256       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_7 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_7[0][0]              
    ==================================================================================================
    Total params: 95,127
    Trainable params: 95,127
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    522/522 [==============================] - 108s 206ms/step - loss: 0.5942 - acc: 0.7398 - val_loss: 0.5514 - val_acc: 0.7962
    Epoch 2/500
    522/522 [==============================] - 108s 208ms/step - loss: 0.4173 - acc: 0.8602 - val_loss: 0.5058 - val_acc: 0.8134
    Epoch 3/500
    522/522 [==============================] - 119s 228ms/step - loss: 0.3810 - acc: 0.8720 - val_loss: 0.4588 - val_acc: 0.8221
    Epoch 4/500
    522/522 [==============================] - 107s 204ms/step - loss: 0.3544 - acc: 0.8821 - val_loss: 0.4412 - val_acc: 0.8331
    Epoch 5/500
    522/522 [==============================] - 112s 214ms/step - loss: 0.3389 - acc: 0.8876 - val_loss: 0.4229 - val_acc: 0.8706
    Epoch 6/500
    522/522 [==============================] - 106s 203ms/step - loss: 0.3309 - acc: 0.8920 - val_loss: 0.4960 - val_acc: 0.8193
    Epoch 7/500
    522/522 [==============================] - 119s 228ms/step - loss: 0.3206 - acc: 0.8949 - val_loss: 0.4554 - val_acc: 0.8215
    Epoch 8/500
    522/522 [==============================] - 106s 203ms/step - loss: 0.3186 - acc: 0.8958 - val_loss: 0.4069 - val_acc: 0.8708
    Epoch 9/500
    522/522 [==============================] - 113s 217ms/step - loss: 0.3076 - acc: 0.8983 - val_loss: 0.4441 - val_acc: 0.8311
    Epoch 10/500
    522/522 [==============================] - 117s 224ms/step - loss: 0.3064 - acc: 0.8983 - val_loss: 0.3907 - val_acc: 0.8615
    Epoch 11/500
    522/522 [==============================] - 124s 238ms/step - loss: 0.2986 - acc: 0.9009 - val_loss: 0.3550 - val_acc: 0.9060
    Epoch 12/500
    522/522 [==============================] - 108s 207ms/step - loss: 0.2991 - acc: 0.9022 - val_loss: 0.3765 - val_acc: 0.8906
    Epoch 13/500
    522/522 [==============================] - 112s 214ms/step - loss: 0.2937 - acc: 0.9013 - val_loss: 0.4141 - val_acc: 0.8464
    Epoch 14/500
    522/522 [==============================] - 124s 237ms/step - loss: 0.2837 - acc: 0.9047 - val_loss: 0.4402 - val_acc: 0.8270
    Epoch 15/500
    522/522 [==============================] - 107s 206ms/step - loss: 0.2917 - acc: 0.9040 - val_loss: 0.4532 - val_acc: 0.8308
    Epoch 16/500
    522/522 [==============================] - 134s 257ms/step - loss: 0.2839 - acc: 0.9067 - val_loss: 0.3325 - val_acc: 0.9061
    Epoch 17/500
    522/522 [==============================] - 108s 206ms/step - loss: 0.2779 - acc: 0.9080 - val_loss: 0.4295 - val_acc: 0.8422
    Epoch 18/500
    522/522 [==============================] - 110s 211ms/step - loss: 0.2767 - acc: 0.9085 - val_loss: 0.3504 - val_acc: 0.8935
    Epoch 19/500
    522/522 [==============================] - 116s 223ms/step - loss: 0.2722 - acc: 0.9084 - val_loss: 0.4239 - val_acc: 0.8502
    Epoch 20/500
    522/522 [==============================] - 110s 211ms/step - loss: 0.2704 - acc: 0.9081 - val_loss: 0.3918 - val_acc: 0.8681
    Epoch 21/500
    522/522 [==============================] - 109s 210ms/step - loss: 0.2689 - acc: 0.9104 - val_loss: 0.4218 - val_acc: 0.8469
    Epoch 22/500
    522/522 [==============================] - 107s 205ms/step - loss: 0.2720 - acc: 0.9089 - val_loss: 0.3991 - val_acc: 0.8571
    


![png](output_44_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.267134606852477, 0.9171422889120893]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.3053543526521871, 0.9322769952491975]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.2748671831706112, 0.8965713009809043]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.26470859021102744, 0.8980158408643577]



# Check Embedding


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '201', time_width, 1024)
nn = SensorCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0001), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    reshape_1 (Reshape)             (None, 120, 24, 1)   0           concatenate_1[0][0]              
    __________________________________________________________________________________________________
    conv2d_1 (Conv2D)               (None, 120, 24, 16)  64          reshape_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_1[0][0]                   
    __________________________________________________________________________________________________
    conv2d_2 (Conv2D)               (None, 120, 24, 16)  6160        leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 120, 24, 16)  0           conv2d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_1 (MaxPooling2D)  (None, 40, 24, 16)   0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv2d_3 (Conv2D)               (None, 40, 24, 32)   1568        max_pooling2d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 40, 24, 32)   0           conv2d_3[0][0]                   
    __________________________________________________________________________________________________
    conv2d_4 (Conv2D)               (None, 40, 24, 32)   24608       leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 40, 24, 32)   0           conv2d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_2 (MaxPooling2D)  (None, 20, 24, 32)   0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv2d_5 (Conv2D)               (None, 20, 24, 32)   3104        max_pooling2d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 20, 24, 32)   0           conv2d_5[0][0]                   
    __________________________________________________________________________________________________
    conv2d_6 (Conv2D)               (None, 20, 24, 64)   49216       leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 20, 24, 64)   0           conv2d_6[0][0]                   
    __________________________________________________________________________________________________
    max_pooling2d_3 (MaxPooling2D)  (None, 10, 1, 64)    0           leaky_re_lu_6[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 640)          0           max_pooling2d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           10256       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_7 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_7[0][0]              
    ==================================================================================================
    Total params: 95,127
    Trainable params: 95,127
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    103/103 [==============================] - 25s 247ms/step - loss: 0.9346 - acc: 0.5937 - val_loss: 0.7554 - val_acc: 0.6945
    Epoch 2/500
    103/103 [==============================] - 22s 215ms/step - loss: 0.5123 - acc: 0.8361 - val_loss: 0.6165 - val_acc: 0.7494
    Epoch 3/500
    103/103 [==============================] - 22s 215ms/step - loss: 0.4076 - acc: 0.8689 - val_loss: 0.5396 - val_acc: 0.7813
    Epoch 4/500
    103/103 [==============================] - 22s 216ms/step - loss: 0.3855 - acc: 0.8832 - val_loss: 0.4704 - val_acc: 0.8436
    Epoch 5/500
    103/103 [==============================] - 22s 217ms/step - loss: 0.3604 - acc: 0.8911 - val_loss: 0.4315 - val_acc: 0.9057
    Epoch 6/500
    103/103 [==============================] - 22s 217ms/step - loss: 0.3525 - acc: 0.8951 - val_loss: 0.4025 - val_acc: 0.9076
    Epoch 7/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3365 - acc: 0.9016 - val_loss: 0.4010 - val_acc: 0.9124
    Epoch 8/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3460 - acc: 0.9013 - val_loss: 0.3816 - val_acc: 0.9114
    Epoch 9/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3190 - acc: 0.9051 - val_loss: 0.3595 - val_acc: 0.9157
    Epoch 10/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3220 - acc: 0.9033 - val_loss: 0.3908 - val_acc: 0.9188
    Epoch 11/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3068 - acc: 0.9091 - val_loss: 0.3544 - val_acc: 0.9024
    Epoch 12/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2965 - acc: 0.9132 - val_loss: 0.3228 - val_acc: 0.9103
    Epoch 13/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.3032 - acc: 0.9090 - val_loss: 0.3448 - val_acc: 0.9084
    Epoch 14/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2983 - acc: 0.9114 - val_loss: 0.3113 - val_acc: 0.9211
    Epoch 15/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2847 - acc: 0.9149 - val_loss: 0.3065 - val_acc: 0.9216
    Epoch 16/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2865 - acc: 0.9162 - val_loss: 0.3103 - val_acc: 0.9155
    Epoch 17/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2837 - acc: 0.9134 - val_loss: 0.3260 - val_acc: 0.9167
    Epoch 18/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2815 - acc: 0.9146 - val_loss: 0.3047 - val_acc: 0.9201
    Epoch 19/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2715 - acc: 0.9169 - val_loss: 0.3037 - val_acc: 0.9201
    Epoch 20/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2711 - acc: 0.9179 - val_loss: 0.3244 - val_acc: 0.9202
    Epoch 21/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2688 - acc: 0.9165 - val_loss: 0.2829 - val_acc: 0.9211
    Epoch 22/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2601 - acc: 0.9199 - val_loss: 0.2918 - val_acc: 0.9237
    Epoch 23/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2597 - acc: 0.9198 - val_loss: 0.2868 - val_acc: 0.9209
    Epoch 24/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2605 - acc: 0.9212 - val_loss: 0.2913 - val_acc: 0.9208
    Epoch 25/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2542 - acc: 0.9190 - val_loss: 0.2998 - val_acc: 0.9145
    Epoch 26/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2470 - acc: 0.9183 - val_loss: 0.2765 - val_acc: 0.9218
    Epoch 27/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2475 - acc: 0.9219 - val_loss: 0.2987 - val_acc: 0.9191
    Epoch 28/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2453 - acc: 0.9194 - val_loss: 0.2896 - val_acc: 0.9217
    Epoch 29/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2386 - acc: 0.9184 - val_loss: 0.3061 - val_acc: 0.9172
    Epoch 30/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2287 - acc: 0.9235 - val_loss: 0.3340 - val_acc: 0.9109
    Epoch 31/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2345 - acc: 0.9216 - val_loss: 0.3022 - val_acc: 0.9178
    Epoch 32/500
    103/103 [==============================] - 22s 218ms/step - loss: 0.2330 - acc: 0.9224 - val_loss: 0.3194 - val_acc: 0.9147
    


![png](output_50_1.png)



```python
rs = RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024)
true = np.argmax(rs.MY[rs.MI+119], axis=1)
y_ = nn.model.predict_generator(rs)
predict = np.argmax(y_, axis=1)
confusion_matrix(true, predict)
```


```python

```




    array([[ 90446,  20692,   1142],
           [  5102, 116181,  17854],
           [  3589,   4617,  61319]])




```python
plt.figure(figsize=(30,3))
plt.scatter(rs.MI[:10000]+119, true[:10000])
plt.figure(figsize=(30,3))
plt.scatter(rs.MI[:10000]+119, predict[:10000]);
```


![png](output_53_0.png)



![png](output_53_1.png)



```python
w = nn.model.get_layer(name='embedding_dayofweek').get_weights()
cmap = plt.cm.tab10
cmap.N = 7
plt.scatter(w[0][:, 0], w[0][:, 1], cmap=cm, c=range(7))
plt.colorbar();
```


![png](output_54_0.png)


# Sensor Depth CNN


```python
class SensorDepthCNN(SensorNN):
    def build_core(self):

        _merged = Concatenate(axis=2)(self.embedding)
        
#         shape = K.int_shape(_merged)[1:2] + (1, ) + K.int_shape(_merged)[1:3]
#         _image = Reshape(shape)(_merged)
        _image = _merged
    
        # Conv 1 #
        _conv = _image

        conv = Conv1D(64, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(3, padding='valid')
        _mpool = mpool(_conv)

        # Conv 2 #
        _conv = _mpool

        conv = Conv1D(128, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(2, padding='valid')
        _mpool = mpool(_conv)

        # Conv 3 #
        _conv = _mpool

        conv = Conv1D(256, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(2, padding='valid')
        _mpool = mpool(_conv)
        
        # Dense #
        flatten = Flatten()
        _flatten = flatten(_mpool)
        
        dense = Dense(16)
        _dense = dense(_flatten)
        _dense = LeakyReLU()(_dense)
        
        dense = Dense(3, activation='softmax')
        _dense = dense(_dense)
        
        self._output = _dense
```


```python
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, '201', time_width, 1024)
nn = SensorDepthCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.00005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_6 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_13 (Conv1D)              (None, 120, 64)      4672        concatenate_6[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_24 (LeakyReLU)      (None, 120, 64)      0           conv1d_13[0][0]                  
    __________________________________________________________________________________________________
    max_pooling1d_13 (MaxPooling1D) (None, 40, 64)       0           leaky_re_lu_24[0][0]             
    __________________________________________________________________________________________________
    conv1d_14 (Conv1D)              (None, 40, 128)      24704       max_pooling1d_13[0][0]           
    __________________________________________________________________________________________________
    leaky_re_lu_25 (LeakyReLU)      (None, 40, 128)      0           conv1d_14[0][0]                  
    __________________________________________________________________________________________________
    max_pooling1d_14 (MaxPooling1D) (None, 20, 128)      0           leaky_re_lu_25[0][0]             
    __________________________________________________________________________________________________
    conv1d_15 (Conv1D)              (None, 20, 256)      98560       max_pooling1d_14[0][0]           
    __________________________________________________________________________________________________
    leaky_re_lu_26 (LeakyReLU)      (None, 20, 256)      0           conv1d_15[0][0]                  
    __________________________________________________________________________________________________
    max_pooling1d_15 (MaxPooling1D) (None, 10, 256)      0           leaky_re_lu_26[0][0]             
    __________________________________________________________________________________________________
    flatten_6 (Flatten)             (None, 2560)         0           max_pooling1d_15[0][0]           
    __________________________________________________________________________________________________
    dense_11 (Dense)                (None, 16)           40976       flatten_6[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_27 (LeakyReLU)      (None, 16)           0           dense_11[0][0]                   
    __________________________________________________________________________________________________
    dense_12 (Dense)                (None, 3)            51          leaky_re_lu_27[0][0]             
    ==================================================================================================
    Total params: 169,063
    Trainable params: 169,063
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    103/103 [==============================] - 3s 28ms/step - loss: 0.7802 - acc: 0.7332 - val_loss: 0.7633 - val_acc: 0.6441
    Epoch 2/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.4691 - acc: 0.8446 - val_loss: 0.7196 - val_acc: 0.7101
    Epoch 3/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.4208 - acc: 0.8618 - val_loss: 0.6693 - val_acc: 0.7553
    Epoch 4/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.3999 - acc: 0.8730 - val_loss: 0.6195 - val_acc: 0.7740
    Epoch 5/500
    103/103 [==============================] - 3s 25ms/step - loss: 0.3672 - acc: 0.8859 - val_loss: 0.5442 - val_acc: 0.8120
    Epoch 6/500
    103/103 [==============================] - 3s 28ms/step - loss: 0.3514 - acc: 0.8965 - val_loss: 0.4586 - val_acc: 0.8630
    Epoch 7/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.3464 - acc: 0.8996 - val_loss: 0.4284 - val_acc: 0.9047
    Epoch 8/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.3246 - acc: 0.9071 - val_loss: 0.3999 - val_acc: 0.9156
    Epoch 9/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.3142 - acc: 0.9083 - val_loss: 0.4091 - val_acc: 0.9156
    Epoch 10/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.3073 - acc: 0.9107 - val_loss: 0.3679 - val_acc: 0.9154
    Epoch 11/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.3024 - acc: 0.9118 - val_loss: 0.3532 - val_acc: 0.9166
    Epoch 12/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2922 - acc: 0.9130 - val_loss: 0.3531 - val_acc: 0.9170
    Epoch 13/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2918 - acc: 0.9140 - val_loss: 0.3474 - val_acc: 0.9168
    Epoch 14/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2826 - acc: 0.9156 - val_loss: 0.3427 - val_acc: 0.9183
    Epoch 15/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2818 - acc: 0.9161 - val_loss: 0.3407 - val_acc: 0.9171
    Epoch 16/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2756 - acc: 0.9173 - val_loss: 0.3242 - val_acc: 0.9178
    Epoch 17/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2724 - acc: 0.9173 - val_loss: 0.3323 - val_acc: 0.9187
    Epoch 18/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2670 - acc: 0.9194 - val_loss: 0.3284 - val_acc: 0.9176
    Epoch 19/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2682 - acc: 0.9179 - val_loss: 0.3210 - val_acc: 0.9177
    Epoch 20/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2695 - acc: 0.9198 - val_loss: 0.3231 - val_acc: 0.9192
    Epoch 21/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2569 - acc: 0.9217 - val_loss: 0.3272 - val_acc: 0.9189
    Epoch 22/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2569 - acc: 0.9220 - val_loss: 0.3241 - val_acc: 0.9189
    Epoch 23/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2495 - acc: 0.9226 - val_loss: 0.3393 - val_acc: 0.9167
    Epoch 24/500
    103/103 [==============================] - 3s 26ms/step - loss: 0.2523 - acc: 0.9217 - val_loss: 0.3240 - val_acc: 0.9177
    Epoch 25/500
    103/103 [==============================] - 3s 27ms/step - loss: 0.2494 - acc: 0.9213 - val_loss: 0.3359 - val_acc: 0.9186
    


![png](output_57_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.2681444967598666, 0.9045950451172726]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [1.6058739312937562, 0.7753521118925211]




```python
nn.model.evaluate_generator(RoomSequence(train_X['203'], train_Y['203'], train_D['203'], time_width=time_width, batch_size=1024))
```




    [1.776722040241656, 0.7473948589432441]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.4849171540484156, 0.8550952061843945]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.5983050076777998, 0.8162502882222414]




```python
K.clear_session()
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorDepthCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 120, 64)      4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 120, 64)      0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 40, 64)       0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 40, 128)      24704       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 40, 128)      0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 20, 128)      0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 20, 256)      98560       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 20, 256)      0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 10, 256)      0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 2560)         0           max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           40976       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_4[0][0]              
    ==================================================================================================
    Total params: 169,063
    Trainable params: 169,063
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    522/522 [==============================] - 31s 59ms/step - loss: 0.4466 - acc: 0.8502 - val_loss: 0.3775 - val_acc: 0.8668
    Epoch 2/500
    522/522 [==============================] - 14s 27ms/step - loss: 0.3280 - acc: 0.8952 - val_loss: 0.3874 - val_acc: 0.8631
    Epoch 3/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2970 - acc: 0.9042 - val_loss: 0.4770 - val_acc: 0.8177
    Epoch 4/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2839 - acc: 0.9068 - val_loss: 0.3593 - val_acc: 0.8895
    Epoch 5/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2750 - acc: 0.9114 - val_loss: 0.3506 - val_acc: 0.8834
    Epoch 6/500
    522/522 [==============================] - 15s 29ms/step - loss: 0.2788 - acc: 0.9090 - val_loss: 0.3500 - val_acc: 0.8817
    Epoch 7/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2614 - acc: 0.9148 - val_loss: 0.3641 - val_acc: 0.8870
    Epoch 8/500
    522/522 [==============================] - 13s 25ms/step - loss: 0.2546 - acc: 0.9112 - val_loss: 0.3500 - val_acc: 0.8933
    Epoch 9/500
    522/522 [==============================] - 15s 28ms/step - loss: 0.2494 - acc: 0.9149 - val_loss: 0.3303 - val_acc: 0.8912
    Epoch 10/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2478 - acc: 0.9151 - val_loss: 0.3045 - val_acc: 0.9118
    Epoch 11/500
    522/522 [==============================] - 13s 25ms/step - loss: 0.2419 - acc: 0.9187 - val_loss: 0.3373 - val_acc: 0.8900
    Epoch 12/500
    522/522 [==============================] - 15s 28ms/step - loss: 0.2401 - acc: 0.9172 - val_loss: 0.3780 - val_acc: 0.8657
    Epoch 13/500
    522/522 [==============================] - 13s 26ms/step - loss: 0.2343 - acc: 0.9198 - val_loss: 0.3464 - val_acc: 0.9012
    Epoch 14/500
    522/522 [==============================] - 15s 30ms/step - loss: 0.2301 - acc: 0.9201 - val_loss: 0.3465 - val_acc: 0.9026
    Epoch 15/500
    522/522 [==============================] - 13s 25ms/step - loss: 0.2310 - acc: 0.9194 - val_loss: 0.3190 - val_acc: 0.8911
    Epoch 16/500
    522/522 [==============================] - 13s 25ms/step - loss: 0.2261 - acc: 0.9193 - val_loss: 0.3672 - val_acc: 0.8714
    


![png](output_63_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.30008839307694035, 0.9216562357700083]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.17766201479334226, 0.9487089193482914]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.3272159075746885, 0.9023251363566719]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.26470859021102744, 0.8980158408643577]




```python
rs = RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024)
true = np.argmax(rs.MY[rs.MI+119], axis=1)
y_ = nn.model.predict_generator(rs)
predict = np.argmax(y_, axis=1)
confusion_matrix(true, predict)
```




    array([[ 97097,    929,   1111],
           [ 18507, 117613,   5370],
           [   494,   1784,  78037]])




```python
index = range(10000, 20000)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+119, true[index])
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+119, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+119, predict[index]);
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+119, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01);
```


![png](output_69_0.png)



![png](output_69_1.png)


## Enlarge time_width


```python
K.clear_session()
time_width = 240
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorDepthCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 240)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 240)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 240)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 240, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 240, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 240, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 240, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 240, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 240, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 240, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 240, 64)      4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 240, 64)      0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 80, 64)       0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 80, 128)      24704       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 80, 128)      0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 40, 128)      0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 40, 256)      98560       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 40, 256)      0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 20, 256)      0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 5120)         0           max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           81936       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_4[0][0]              
    ==================================================================================================
    Total params: 210,023
    Trainable params: 210,023
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    501/501 [==============================] - 23s 46ms/step - loss: 0.4673 - acc: 0.8393 - val_loss: 0.4659 - val_acc: 0.8388
    Epoch 2/500
    501/501 [==============================] - 25s 50ms/step - loss: 0.3412 - acc: 0.8924 - val_loss: 0.3883 - val_acc: 0.8868
    Epoch 3/500
    501/501 [==============================] - 28s 56ms/step - loss: 0.3046 - acc: 0.9032 - val_loss: 0.3559 - val_acc: 0.9105
    Epoch 4/500
    501/501 [==============================] - 23s 45ms/step - loss: 0.2872 - acc: 0.9084 - val_loss: 0.3075 - val_acc: 0.9123
    Epoch 5/500
    501/501 [==============================] - 22s 44ms/step - loss: 0.2864 - acc: 0.9092 - val_loss: 0.3437 - val_acc: 0.8921
    Epoch 6/500
    501/501 [==============================] - 21s 42ms/step - loss: 0.2712 - acc: 0.9113 - val_loss: 0.3163 - val_acc: 0.9142
    Epoch 7/500
    501/501 [==============================] - 21s 42ms/step - loss: 0.2547 - acc: 0.9149 - val_loss: 0.3038 - val_acc: 0.9043
    Epoch 8/500
    501/501 [==============================] - 22s 44ms/step - loss: 0.2534 - acc: 0.9159 - val_loss: 0.4455 - val_acc: 0.8547
    Epoch 9/500
    501/501 [==============================] - 22s 44ms/step - loss: 0.2511 - acc: 0.9154 - val_loss: 0.3156 - val_acc: 0.9145
    Epoch 10/500
    501/501 [==============================] - 21s 42ms/step - loss: 0.2411 - acc: 0.9175 - val_loss: 0.3362 - val_acc: 0.9071
    Epoch 11/500
    501/501 [==============================] - 24s 48ms/step - loss: 0.2391 - acc: 0.9182 - val_loss: 0.3633 - val_acc: 0.8834
    Epoch 12/500
    501/501 [==============================] - 24s 48ms/step - loss: 0.2342 - acc: 0.9218 - val_loss: 0.5041 - val_acc: 0.8419
    Epoch 13/500
    501/501 [==============================] - 24s 48ms/step - loss: 0.2291 - acc: 0.9212 - val_loss: 0.3514 - val_acc: 0.8886
    


![png](output_71_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.25646193267385864, 0.9207693031425861]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.21223694435239018, 0.9412945282860012]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.26737524364303944, 0.9033804953662478]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.21848551949280803, 0.911649400144341]




```python
rs = RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024)
true = np.argmax(rs.MY[rs.MI+time_width-1], axis=1)
y_ = nn.model.predict_generator(rs)
predict = np.argmax(y_, axis=1)
confusion_matrix(true, predict)
```




    array([[ 90270,   2042,     86],
           [ 17196, 112249,   5004],
           [   370,   2413,  77227]])




```python
index = range(10000, 20000)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+time_width-1, true[index])
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+time_width-1, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+time_width-1, predict[index]);
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+time_width-1, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01);
```


![png](output_77_0.png)



![png](output_77_1.png)


## Further more ...


```python
K.clear_session()
time_width = 720
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 128)
nn = SensorDepthCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 720)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 720)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 720)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 720, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 720, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 720, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 720, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 720, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 720, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 720, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 720, 64)      4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 720, 64)      0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 240, 64)      0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 240, 128)     24704       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 240, 128)     0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 120, 128)     0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 120, 256)     98560       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 120, 256)     0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 60, 256)      0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 15360)        0           max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 16)           245776      flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 16)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            51          leaky_re_lu_4[0][0]              
    ==================================================================================================
    Total params: 373,863
    Trainable params: 373,863
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    3374/3374 [==============================] - 108s 32ms/step - loss: 0.4194 - acc: 0.8542 - val_loss: 0.5000 - val_acc: 0.8287
    Epoch 2/500
    3374/3374 [==============================] - 65s 19ms/step - loss: 0.3030 - acc: 0.8974 - val_loss: 0.3696 - val_acc: 0.8567
    Epoch 3/500
    3374/3374 [==============================] - 161s 48ms/step - loss: 0.2762 - acc: 0.9028 - val_loss: 0.3726 - val_acc: 0.8848
    Epoch 4/500
    3374/3374 [==============================] - 169s 50ms/step - loss: 0.2515 - acc: 0.9115 - val_loss: 0.3749 - val_acc: 0.8828
    Epoch 5/500
    3374/3374 [==============================] - 63s 19ms/step - loss: 0.2366 - acc: 0.9172 - val_loss: 0.4270 - val_acc: 0.8595
    Epoch 6/500
    3374/3374 [==============================] - 246s 73ms/step - loss: 0.2245 - acc: 0.9179 - val_loss: 0.4139 - val_acc: 0.8497
    Epoch 7/500
    3374/3374 [==============================] - 182s 54ms/step - loss: 0.2115 - acc: 0.9233 - val_loss: 0.4728 - val_acc: 0.8424
    Epoch 8/500
    3374/3374 [==============================] - 64s 19ms/step - loss: 0.1896 - acc: 0.9321 - val_loss: 0.5823 - val_acc: 0.8043
    


![png](output_79_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.3461509069990142, 0.8687981617471665]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.25460460303669497, 0.936026936026936]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.3339252455670943, 0.8489770765172583]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.21174986275194702, 0.9253233339704079]




```python
rs = RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024)
true = np.argmax(rs.MY[rs.MI+time_width-1], axis=1)
y_ = nn.model.predict_generator(rs)
predict = np.argmax(y_, axis=1)
confusion_matrix(true, predict)
```




    array([[ 61237,   8291,      0],
           [  4158, 105713,   4954],
           [     1,   1575,  68220]])




```python
index = range(20000, 30000)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+time_width-1, true[index])
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+time_width-1, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01)

plt.figure(figsize=(30,3))
plt.scatter(rs.MI[index]+time_width-1, predict[index]);
plt.gca().twinx()
plt.yticks([], [])
plt.scatter(rs.MI[index]+time_width-1, pd.DatetimeIndex(rs.MD[rs.MI[index]]).time, c='green', alpha=0.01);
```


![png](output_85_0.png)



![png](output_85_1.png)


# More Convolution


```python
class SensorDepthMCNN(SensorNN):
    def build_core(self):

        _merged = Concatenate(axis=2)(self.embedding)
        
#         shape = K.int_shape(_merged)[1:2] + (1, ) + K.int_shape(_merged)[1:3]
#         _image = Reshape(shape)(_merged)
        _image = _merged
    
        # Conv 1 #
        _conv = _image

        conv = Conv1D(64, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(3, padding='valid')
        _mpool = mpool(_conv)

        # Conv 2 #
        _conv = _mpool

        conv = Conv1D(96, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(2, padding='valid')
        _mpool = mpool(_conv)

        # Conv 3 #
        _conv = _mpool

        conv = Conv1D(128, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(2, padding='valid')
        _mpool = mpool(_conv)
        
        # Conv 4 #
        _conv = _mpool

        conv = Conv1D(192, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(2, padding='valid')
        _mpool = mpool(_conv)
        
        # Conv 5 #
        _conv = _mpool

        conv = Conv1D(256, 3, padding='same')
        _conv = conv(_conv)
        _conv = LeakyReLU()(_conv)

        mpool = MaxPooling1D(5, padding='valid')
        _mpool = mpool(_conv)
    
        # Conv 5 #
        flatten = Flatten()
        _flatten = flatten(_mpool)
        
        dense = Dense(64)
        _dense = dense(_flatten)
        _dense = LeakyReLU()(_dense)
        
        dense = Dense(3, activation='softmax')
        _dense = dense(_dense)
        
        self._output = _dense
```


```python
K.clear_session()
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorDepthMCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 120, 64)      4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 120, 64)      0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 40, 64)       0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 40, 96)       18528       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 40, 96)       0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 20, 96)       0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 20, 128)      36992       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 20, 128)      0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 10, 128)      0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    conv1d_4 (Conv1D)               (None, 10, 192)      73920       max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 10, 192)      0           conv1d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_4 (MaxPooling1D)  (None, 5, 192)       0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv1d_5 (Conv1D)               (None, 5, 256)       147712      max_pooling1d_4[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 5, 256)       0           conv1d_5[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_5 (MaxPooling1D)  (None, 1, 256)       0           leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 256)          0           max_pooling1d_5[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 64)           16448       flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 64)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            195         leaky_re_lu_6[0][0]              
    ==================================================================================================
    Total params: 298,567
    Trainable params: 298,567
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    522/522 [==============================] - 16s 31ms/step - loss: 0.4627 - acc: 0.8362 - val_loss: 0.3968 - val_acc: 0.8600
    Epoch 2/500
    522/522 [==============================] - 39s 75ms/step - loss: 0.3180 - acc: 0.9000 - val_loss: 0.4206 - val_acc: 0.8821
    Epoch 3/500
    522/522 [==============================] - 17s 32ms/step - loss: 0.3035 - acc: 0.9043 - val_loss: 0.3717 - val_acc: 0.8763
    Epoch 4/500
    522/522 [==============================] - 16s 30ms/step - loss: 0.2917 - acc: 0.9094 - val_loss: 0.3326 - val_acc: 0.9098
    Epoch 5/500
    522/522 [==============================] - 15s 29ms/step - loss: 0.2793 - acc: 0.9135 - val_loss: 0.3955 - val_acc: 0.8732
    Epoch 6/500
    522/522 [==============================] - 15s 29ms/step - loss: 0.2685 - acc: 0.9160 - val_loss: 0.3025 - val_acc: 0.9133
    Epoch 7/500
    522/522 [==============================] - 15s 30ms/step - loss: 0.2710 - acc: 0.9151 - val_loss: 0.3073 - val_acc: 0.9150
    Epoch 8/500
    522/522 [==============================] - 15s 29ms/step - loss: 0.2638 - acc: 0.9153 - val_loss: 0.3129 - val_acc: 0.9160
    Epoch 9/500
    522/522 [==============================] - 16s 30ms/step - loss: 0.2593 - acc: 0.9168 - val_loss: 0.3112 - val_acc: 0.9131
    Epoch 10/500
    522/522 [==============================] - 15s 28ms/step - loss: 0.2516 - acc: 0.9180 - val_loss: 0.5171 - val_acc: 0.7952
    Epoch 11/500
    522/522 [==============================] - 15s 29ms/step - loss: 0.2491 - acc: 0.9185 - val_loss: 0.3632 - val_acc: 0.8888
    Epoch 12/500
    522/522 [==============================] - 16s 31ms/step - loss: 0.2387 - acc: 0.9186 - val_loss: 0.3376 - val_acc: 0.9027
    


![png](output_88_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.24001777255323806, 0.9273790033704921]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.29885149197959004, 0.9153755879178294]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.31723549988875166, 0.9016999968105126]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.2578722644997073, 0.9095319403639647]




```python
rs = RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024)
true = np.argmax(rs.MY[rs.MI+119], axis=1)
y_ = nn.model.predict_generator(rs)
predict = np.argmax(y_, axis=1)
confusion_matrix(true, predict)
```




    array([[ 98595,    542,      0],
           [ 20230, 116151,   5109],
           [  1284,   1870,  77161]])




```python
index = range(300000, 305000)

plt.figure(figsize=(30,3))
plt.scatter(pd.DatetimeIndex(rs.MD[rs.MI[index]+119]), true[index])
plt.figure(figsize=(30,3))
plt.scatter(pd.DatetimeIndex(rs.MD[rs.MI[index]+119]), predict[index])

```




    <matplotlib.collections.PathCollection at 0x7f898937ab38>




![png](output_94_1.png)



![png](output_94_2.png)



```python
di = rs.MD[rs.MI[index]+119]
sn = homeiot.SensorNet(room="205").load_df(di[0], di[-1])
sn.slice().viz("2D")
```


```python
sn
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>id</th>
      <th>category</th>
      <th>kind</th>
      <th>wired</th>
      <th>tag</th>
      <th>table</th>
      <th>id_col</th>
      <th>id_val</th>
      <th>datetime_col</th>
      <th>m2m_id</th>
      <th>room</th>
      <th>place</th>
      <th>serial_id</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>9</th>
      <td>PIR010</td>
      <td>PIR</td>
      <td>PIR</td>
      <td>False</td>
      <td>ermine_etc-pir</td>
      <td>SDStore_ermine_ETC-PIR</td>
      <td>sensorId</td>
      <td>PIR010</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room</td>
      <td>04015731</td>
    </tr>
    <tr>
      <th>30</th>
      <td>WTK013</td>
      <td>PIR</td>
      <td>WTK</td>
      <td>True</td>
      <td>panasonic_wtk23111</td>
      <td>SDStore_Panasonic_WTK23111</td>
      <td>sensorId</td>
      <td>WTK013</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>45</th>
      <td>RHT010</td>
      <td>RHT</td>
      <td>RHT</td>
      <td>False</td>
      <td>ermine_etb-rht</td>
      <td>SDStore_ermine_ETB-RHT</td>
      <td>sensorId</td>
      <td>RHT010</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room</td>
      <td>0401514E</td>
    </tr>
    <tr>
      <th>64</th>
      <td>ILL010</td>
      <td>ILL</td>
      <td>ILL</td>
      <td>False</td>
      <td>ermine_etb-ill</td>
      <td>SDStore_ermine_ETB-ILL</td>
      <td>sensorId</td>
      <td>ILL010</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room</td>
      <td>040179D2</td>
    </tr>
    <tr>
      <th>91</th>
      <td>OCS020</td>
      <td>OCS</td>
      <td>OCS</td>
      <td>False</td>
      <td>ermine_etb-ocs</td>
      <td>SDStore_ermine_ETB-OCS</td>
      <td>sensorId</td>
      <td>OCS020</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>entrance door</td>
      <td>04007E22</td>
    </tr>
    <tr>
      <th>92</th>
      <td>OCS021</td>
      <td>OCS</td>
      <td>OCS</td>
      <td>False</td>
      <td>ermine_etb-ocs</td>
      <td>SDStore_ermine_ETB-OCS</td>
      <td>sensorId</td>
      <td>OCS021</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room window E</td>
      <td>04007EB2</td>
    </tr>
    <tr>
      <th>93</th>
      <td>OCS022</td>
      <td>OCS</td>
      <td>OCS</td>
      <td>False</td>
      <td>ermine_etb-ocs</td>
      <td>SDStore_ermine_ETB-OCS</td>
      <td>sensorId</td>
      <td>OCS022</td>
      <td>getDatetime</td>
      <td>M2M006</td>
      <td>205</td>
      <td>room window N</td>
      <td>04007E9C</td>
    </tr>
    <tr>
      <th>107</th>
      <td>PND005</td>
      <td>OCS</td>
      <td>PND</td>
      <td>True</td>
      <td>panasonic_rireki</td>
      <td>SDStore_Panasonic_rireki</td>
      <td>operationTarget</td>
      <td>(0101010005)205号室</td>
      <td>occurrenceDatetime</td>
      <td>NaN</td>
      <td>205</td>
      <td>entrance door</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
w = nn.model.get_layer(name='embedding_dayofweek').get_weights()
cmap = plt.cm.tab10
cmap.N = 7
plt.scatter(w[0][:, 0], w[0][:, 1], cmap=cm, c=range(7))
plt.colorbar();
```


![png](output_97_0.png)



```python
W = nn.model.get_layer(name='conv1d_1').get_weights()[0]
for i in range(W.shape[1]):
    w = W[:, i, :]
    plt.figure(figsize=(30, 3))
    sns.heatmap(w[:, w[0].argsort()], cmap='binary');
```

    /home/eguchi/.pyenv/versions/anaconda3-5.2.0/lib/python3.6/site-packages/matplotlib/pyplot.py:537: RuntimeWarning: More than 20 figures have been opened. Figures created through the pyplot interface (`matplotlib.pyplot.figure`) are retained until explicitly closed and may consume too much memory. (To control this warning, see the rcParam `figure.max_open_warning`).
      max_open_warning, RuntimeWarning)
    


![png](output_98_1.png)



![png](output_98_2.png)



![png](output_98_3.png)



![png](output_98_4.png)



![png](output_98_5.png)



![png](output_98_6.png)



![png](output_98_7.png)



![png](output_98_8.png)



![png](output_98_9.png)



![png](output_98_10.png)



![png](output_98_11.png)



![png](output_98_12.png)



![png](output_98_13.png)



![png](output_98_14.png)



![png](output_98_15.png)



![png](output_98_16.png)



![png](output_98_17.png)



![png](output_98_18.png)



![png](output_98_19.png)



![png](output_98_20.png)



![png](output_98_21.png)



![png](output_98_22.png)



![png](output_98_23.png)



![png](output_98_24.png)


# Dilated CNN


```python
np.log(1024) / np.log(2)
```




    10.0




```python
class SensorDepthDCNN(SensorNN):
    def build_core(self):

        _merged = Concatenate(axis=2)(self.embedding)
        
        # Conv #
        _conv = _merged
        power = np.log(self.time_width) / np.log(2)
        for i in range(int(power)):
            conv = Conv1D(64, 2, dilation_rate=2**i, padding='causal')
            _conv = conv(_conv)
            _conv = LeakyReLU()(_conv)

        
        cropping = Cropping1D((self.time_width-1, 0))# Only last
        _cropping = cropping(_conv)
        
        flatten = Flatten()
        _flatten = flatten(_cropping)
        
        dense = Dense(64)
        _dense = dense(_flatten)
        _dense = LeakyReLU()(_dense)
        
        dense = Dense(3, activation='softmax')
        _dense = dense(_dense)
        
        self._output = _dense
```


```python
K.clear_session()
time_width = 2 ** 7
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorDepthDCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```


```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.2735087307456842, 0.9094392253953592]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.22101388521302523, 0.9382672324509301]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.2842316944986027, 0.9015669252759965]




```python
nn.model.evaluate_generator(RoomSequence(train_X['205'], train_Y['205'], train_D['205'], time_width=time_width, batch_size=1024))
```




    [0.2827561227818466, 0.8997787098878144]



### Enlarge time_width ...


```python
K.clear_session()
time_width = 2 ** 8
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 128)
nn = SensorDepthDCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0002), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 256)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 256)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 256)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 256, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 256, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 256, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 256, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 256, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 256, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 256, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 256, 64)      3136        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 256, 64)      0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 256, 64)      0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 256, 64)      0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    conv1d_4 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 256, 64)      0           conv1d_4[0][0]                   
    __________________________________________________________________________________________________
    conv1d_5 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 256, 64)      0           conv1d_5[0][0]                   
    __________________________________________________________________________________________________
    conv1d_6 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 256, 64)      0           conv1d_6[0][0]                   
    __________________________________________________________________________________________________
    conv1d_7 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_6[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_7 (LeakyReLU)       (None, 256, 64)      0           conv1d_7[0][0]                   
    __________________________________________________________________________________________________
    conv1d_8 (Conv1D)               (None, 256, 64)      8256        leaky_re_lu_7[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_8 (LeakyReLU)       (None, 256, 64)      0           conv1d_8[0][0]                   
    __________________________________________________________________________________________________
    cropping1d_1 (Cropping1D)       (None, 1, 64)        0           leaky_re_lu_8[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 64)           0           cropping1d_1[0][0]               
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 64)           4160        flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_9 (LeakyReLU)       (None, 64)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            195         leaky_re_lu_9[0][0]              
    ==================================================================================================
    Total params: 65,383
    Trainable params: 65,383
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    3980/3980 [==============================] - 105s 26ms/step - loss: 0.4539 - acc: 0.8395 - val_loss: 0.5591 - val_acc: 0.8105
    Epoch 2/500
    3980/3980 [==============================] - 106s 27ms/step - loss: 0.3379 - acc: 0.8863 - val_loss: 0.5614 - val_acc: 0.8176
    Epoch 3/500
    3980/3980 [==============================] - 108s 27ms/step - loss: 0.3047 - acc: 0.8979 - val_loss: 0.3933 - val_acc: 0.8932
    Epoch 4/500
    3980/3980 [==============================] - 108s 27ms/step - loss: 0.2847 - acc: 0.9055 - val_loss: 0.3759 - val_acc: 0.8931
    Epoch 5/500
    3980/3980 [==============================] - 106s 27ms/step - loss: 0.2701 - acc: 0.9080 - val_loss: 0.3682 - val_acc: 0.9007
    Epoch 6/500
    3980/3980 [==============================] - 106s 27ms/step - loss: 0.2544 - acc: 0.9124 - val_loss: 0.4796 - val_acc: 0.8455
    Epoch 7/500
    3980/3980 [==============================] - 108s 27ms/step - loss: 0.2525 - acc: 0.9126 - val_loss: 0.4139 - val_acc: 0.8731
    Epoch 8/500
    3980/3980 [==============================] - 105s 26ms/step - loss: 0.2392 - acc: 0.9175 - val_loss: 0.5181 - val_acc: 0.8064
    Epoch 9/500
    3978/3980 [============================>.] - ETA: 0s - loss: 0.2367 - acc: 0.9178


```python
nn
```




    <__main__.SensorDepthDCNN at 0x7f1f5dc41d30>




```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.3682067432017037, 0.9042850415947261]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.18791030173236575, 0.9503034908902964]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.3610800722877942, 0.8851487903378475]



# NOT ReLU, Use Softsign (tanh) and Sigmoid


```python
# TODO: softsign and sigmoid, stack blocks
```

# LSTM ...


```python
class SensorLSTM(SensorNN):
    def build_core(self):

        _merged = Concatenate(axis=2)(self.embedding)
        
        # LSTM #
        _lstm = _merged
        
        lstm = LSTM(256, return_state=True)
        _h, _c = lstm(_lstm)[1:]
        act = LeakyReLU()
        _h, _c = act(_h), act(_c)
        
        # Dense #
        _dense = Concatenate(axis=1)([_h, _c])
        
        dense = Dense(3, activation='softmax')
        _dense = dense(_dense)
        
        self._output = _dense
```


```python
K.clear_session()
time_width = 120
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201', '203','205'], time_width, 1024)
nn = SensorLSTM(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0005), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    lstm_1 (LSTM)                   [(None, 256), (None, 287744      concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 256)          0           lstm_1[0][1]                     
                                                                     lstm_1[0][2]                     
    __________________________________________________________________________________________________
    concatenate_2 (Concatenate)     (None, 512)          0           leaky_re_lu_1[0][0]              
                                                                     leaky_re_lu_1[1][0]              
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 3)            1539        concatenate_2[0][0]              
    ==================================================================================================
    Total params: 289,383
    Trainable params: 289,383
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    522/522 [==============================] - 64s 122ms/step - loss: 0.4820 - acc: 0.8366 - val_loss: 0.6615 - val_acc: 0.8182
    Epoch 2/500
    522/522 [==============================] - 67s 128ms/step - loss: 0.3773 - acc: 0.8696 - val_loss: 0.7487 - val_acc: 0.8180
    Epoch 3/500
    522/522 [==============================] - 66s 127ms/step - loss: 0.3333 - acc: 0.8878 - val_loss: 0.3721 - val_acc: 0.8681
    Epoch 4/500
    522/522 [==============================] - 69s 133ms/step - loss: 0.3037 - acc: 0.8981 - val_loss: 0.6021 - val_acc: 0.8227
    Epoch 5/500
    522/522 [==============================] - 64s 124ms/step - loss: 0.3186 - acc: 0.8958 - val_loss: 0.5712 - val_acc: 0.8347
    Epoch 6/500
    522/522 [==============================] - 66s 126ms/step - loss: 0.2924 - acc: 0.9034 - val_loss: 0.3908 - val_acc: 0.8519
    Epoch 7/500
    522/522 [==============================] - 68s 131ms/step - loss: 0.2878 - acc: 0.9064 - val_loss: 0.3840 - val_acc: 0.8791
    Epoch 8/500
    522/522 [==============================] - 75s 143ms/step - loss: 0.2872 - acc: 0.9066 - val_loss: 0.3601 - val_acc: 0.8883
    Epoch 9/500
    522/522 [==============================] - 64s 123ms/step - loss: 0.2738 - acc: 0.9127 - val_loss: 0.5023 - val_acc: 0.8538
    Epoch 10/500
    522/522 [==============================] - 64s 123ms/step - loss: 0.2809 - acc: 0.9104 - val_loss: 0.5172 - val_acc: 0.8451
    Epoch 11/500
    522/522 [==============================] - 64s 123ms/step - loss: 0.2755 - acc: 0.9105 - val_loss: 0.7295 - val_acc: 0.8200
    Epoch 12/500
    522/522 [==============================] - 68s 130ms/step - loss: 0.2770 - acc: 0.9107 - val_loss: 0.4768 - val_acc: 0.8677
    Epoch 13/500
    522/522 [==============================] - 96s 183ms/step - loss: 0.2753 - acc: 0.9109 - val_loss: 0.3757 - val_acc: 0.8917
    Epoch 14/500
    522/522 [==============================] - 69s 133ms/step - loss: 0.2783 - acc: 0.9097 - val_loss: 0.3228 - val_acc: 0.9067
    Epoch 15/500
    522/522 [==============================] - 80s 152ms/step - loss: 0.2662 - acc: 0.9137 - val_loss: 0.3342 - val_acc: 0.9029
    Epoch 16/500
    522/522 [==============================] - 79s 151ms/step - loss: 0.2813 - acc: 0.9061 - val_loss: 0.3902 - val_acc: 0.8710
    Epoch 17/500
    522/522 [==============================] - 83s 159ms/step - loss: 0.2589 - acc: 0.9146 - val_loss: 0.3478 - val_acc: 0.9116
    Epoch 18/500
    522/522 [==============================] - 63s 121ms/step - loss: 0.2516 - acc: 0.9159 - val_loss: 0.3859 - val_acc: 0.8691
    Epoch 19/500
    522/522 [==============================] - 70s 134ms/step - loss: 0.2519 - acc: 0.9164 - val_loss: 0.3354 - val_acc: 0.8909
    Epoch 20/500
    522/522 [==============================] - 72s 138ms/step - loss: 0.2396 - acc: 0.9178 - val_loss: 0.3926 - val_acc: 0.8817
    


![png](output_117_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.28447043250526216, 0.908665248642241]




```python
nn.model.evaluate_generator(RoomSequence(test_X['203'], test_Y['203'], test_D['203'], time_width=time_width, batch_size=1024))
```




    [0.26100746236496686, 0.9272300463886888]




```python
nn.model.evaluate_generator(RoomSequence(test_X['205'], test_Y['205'], test_D['205'], time_width=time_width, batch_size=1024))
```




    [0.27208015487126513, 0.9041559021526085]



# Washing Machine


```python
def get_wash_data(room_num, time_width=120, time_width_margin=1440, funcs=[get_lpp_data, get_month, get_dayofweek, get_day, get_clock]):
    df = homeiot.WashingMachine(room_num).load_df('2017-03-01', '2018-06-01').parse().df
    df['count'].fillna(method='ffill', inplace=True)
    df['time_left'].fillna(method='bfill', inplace=True)
    index = df[df['count'].diff() > 0].index
    data = pd.Series(index=index.floor('min'), data=1)
    data = pd.concat([pd.Series({(data.index[0]-pd.Timedelta('1min')*time_width_margin):0}), data])
    data = data.resample('min').asfreq().fillna(0)
    y = data.values
    d = data.index.tz_localize(None)
    x = homeiot.SensorNet(room=room_num).drop(place='window').load_df(d[0]-pd.Timedelta('60min'), d[-1]+pd.Timedelta('60min')).slice().getX(index=d)
    xa = [x]
    for func in funcs:
        xa.append(func(d))
    return RoomSequence([xa], [y], [d], time_width=120, batch_size=128)
```


```python
rs = get_wash_data('201')
```


```python
m = nn.model
last = 12
for i in range(last+1):
    m.get_layer(index=i).trainable = False
mm = Model(m.input, Dense(1, activation='sigmoid')(m.get_layer(index=last).output))
mm.compile(optimizer=Adam(0.0001), loss='binary_crossentropy', metrics=['accuracy'])
mm.summary()
mm.fit_generator(rs, shuffle=True, max_queue_size=1024, epochs=3)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 120)          0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 120, 15)      0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 120, 1)       0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 120, 2)       24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 120, 2)       14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 120, 2)       62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 120, 2)       0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 120, 24)      0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    lstm_1 (LSTM)                   [(None, 256), (None, 287744      concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 256)          0           lstm_1[0][1]                     
                                                                     lstm_1[0][2]                     
    __________________________________________________________________________________________________
    concatenate_2 (Concatenate)     (None, 512)          0           leaky_re_lu_1[0][0]              
                                                                     leaky_re_lu_1[1][0]              
    __________________________________________________________________________________________________
    dense_10 (Dense)                (None, 1)            513         concatenate_2[0][0]              
    ==================================================================================================
    Total params: 288,357
    Trainable params: 513
    Non-trainable params: 287,844
    __________________________________________________________________________________________________
    Epoch 1/3
    3804/3804 [==============================] - 75s 20ms/step - loss: 0.1500 - acc: 0.9786
    Epoch 2/3
    3804/3804 [==============================] - 72s 19ms/step - loss: 0.0250 - acc: 0.9996
    Epoch 3/3
    3804/3804 [==============================] - 72s 19ms/step - loss: 0.0122 - acc: 0.9996
    




    <keras.callbacks.History at 0x7fda7b773518>




```python
rs.MD.shape
```




    (486930,)




```python
predict.shape
```




    (486811, 1)




```python
predict = mm.predict_generator(rs)
```


```python
rs.MD[119:][slice(1,5)]
```




    array(['2017-06-25T21:46:00.000000000', '2017-06-25T21:47:00.000000000',
           '2017-06-25T21:48:00.000000000', '2017-06-25T21:49:00.000000000'],
          dtype='datetime64[ns]')




```python
for _ in range(20, 30):
    s = slice(1440*_*4, 1440*(_+1)*4)
    plt.figure(figsize=(30,3))
    plt.plot(rs.MD[119:][s], predict[s], alpha=0.6)
    plt.figure(figsize=(30,3))
    plt.plot(rs.MD[119:][s], rs.MY[119:][s], alpha=0.6, c='r')
```


![png](output_129_0.png)



![png](output_129_1.png)



![png](output_129_2.png)



![png](output_129_3.png)



![png](output_129_4.png)



![png](output_129_5.png)



![png](output_129_6.png)



![png](output_129_7.png)



![png](output_129_8.png)



![png](output_129_9.png)



![png](output_129_10.png)



![png](output_129_11.png)



![png](output_129_12.png)



![png](output_129_13.png)



![png](output_129_14.png)



![png](output_129_15.png)



![png](output_129_16.png)



![png](output_129_17.png)



![png](output_129_18.png)



![png](output_129_19.png)


# How about CNN?


```python
K.clear_session()
time_width = 1440
train_seq, val_seq = test_val_split(train_X, train_Y, train_D, ['201'], time_width, 1024)
nn = SensorDepthMCNN(time_width=time_width, sensor_ndim=15).compile(optimizer=Adam(0.0002), loss='categorical_crossentropy')
nn.model.summary()
nn.fit_generator(train_seq, validation_data=val_seq, shuffle=True)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 1440, 15)     0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 1440, 1)      0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 1440, 2)      24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 1440, 2)      14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 1440, 2)      62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 1440, 2)      0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 1440, 24)     0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 1440, 64)     4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 1440, 64)     0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 480, 64)      0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 480, 96)      18528       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 480, 96)      0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 240, 96)      0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 240, 128)     36992       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 240, 128)     0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 120, 128)     0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    conv1d_4 (Conv1D)               (None, 120, 192)     73920       max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 120, 192)     0           conv1d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_4 (MaxPooling1D)  (None, 60, 192)      0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv1d_5 (Conv1D)               (None, 60, 256)      147712      max_pooling1d_4[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 60, 256)      0           conv1d_5[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_5 (MaxPooling1D)  (None, 12, 256)      0           leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 3072)         0           max_pooling1d_5[0][0]            
    __________________________________________________________________________________________________
    dense_1 (Dense)                 (None, 64)           196672      flatten_1[0][0]                  
    __________________________________________________________________________________________________
    leaky_re_lu_6 (LeakyReLU)       (None, 64)           0           dense_1[0][0]                    
    __________________________________________________________________________________________________
    dense_2 (Dense)                 (None, 3)            195         leaky_re_lu_6[0][0]              
    ==================================================================================================
    Total params: 478,791
    Trainable params: 478,791
    Non-trainable params: 0
    __________________________________________________________________________________________________
    Epoch 1/500
    71/71 [==============================] - 28s 391ms/step - loss: 0.7279 - acc: 0.6914 - val_loss: 0.9499 - val_acc: 0.6092
    Epoch 2/500
    71/71 [==============================] - 17s 244ms/step - loss: 0.5333 - acc: 0.7895 - val_loss: 0.5227 - val_acc: 0.8815
    Epoch 3/500
    71/71 [==============================] - 18s 251ms/step - loss: 0.4421 - acc: 0.8427 - val_loss: 0.4694 - val_acc: 0.8940
    Epoch 4/500
    71/71 [==============================] - 18s 258ms/step - loss: 0.3773 - acc: 0.8637 - val_loss: 0.3948 - val_acc: 0.8964
    Epoch 5/500
    71/71 [==============================] - 18s 258ms/step - loss: 0.3584 - acc: 0.8702 - val_loss: 0.3365 - val_acc: 0.9072
    Epoch 6/500
    71/71 [==============================] - 17s 240ms/step - loss: 0.3173 - acc: 0.8946 - val_loss: 0.3432 - val_acc: 0.9126
    Epoch 7/500
    71/71 [==============================] - 23s 320ms/step - loss: 0.3002 - acc: 0.9018 - val_loss: 0.3316 - val_acc: 0.9080
    Epoch 8/500
    71/71 [==============================] - 19s 269ms/step - loss: 0.2674 - acc: 0.9092 - val_loss: 0.3867 - val_acc: 0.9090
    Epoch 9/500
    71/71 [==============================] - 19s 265ms/step - loss: 0.2570 - acc: 0.9093 - val_loss: 0.5580 - val_acc: 0.8042
    Epoch 10/500
    71/71 [==============================] - 19s 267ms/step - loss: 0.2802 - acc: 0.8998 - val_loss: 0.3687 - val_acc: 0.9092
    Epoch 11/500
    71/71 [==============================] - 17s 234ms/step - loss: 0.2115 - acc: 0.9219 - val_loss: 0.3795 - val_acc: 0.9152
    Epoch 12/500
    71/71 [==============================] - 29s 402ms/step - loss: 0.1971 - acc: 0.9381 - val_loss: 0.4529 - val_acc: 0.9095
    Epoch 13/500
    71/71 [==============================] - 18s 249ms/step - loss: 0.2023 - acc: 0.9270 - val_loss: 0.5602 - val_acc: 0.9029
    


![png](output_131_1.png)



```python
nn.model.evaluate_generator(RoomSequence(test_X['201'], test_Y['201'], test_D['201'], time_width=time_width, batch_size=1024))
```




    [0.45629553801728107, 0.8730755887182294]




```python
m = nn.model
rs.set_time_width(time_width)
last = 25
for i in range(last+1):
    m.get_layer(index=i).trainable = False
mm = Model(m.input, Dense(1, activation='sigmoid')(m.get_layer(index=last).output))
mm.compile(optimizer=Adam(0.0001), loss='binary_crossentropy', metrics=['accuracy'])
mm.summary()
mm.fit_generator(rs, shuffle=True, max_queue_size=1024, epochs=5)
```

    __________________________________________________________________________________________________
    Layer (type)                    Output Shape         Param #     Connected to                     
    ==================================================================================================
    input_month (InputLayer)        (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_dayofweek (InputLayer)    (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_day (InputLayer)          (None, 1440)         0                                            
    __________________________________________________________________________________________________
    input_sensor (InputLayer)       (None, 1440, 15)     0                                            
    __________________________________________________________________________________________________
    input_common (InputLayer)       (None, 1440, 1)      0                                            
    __________________________________________________________________________________________________
    embedding_month (Embedding)     (None, 1440, 2)      24          input_month[0][0]                
    __________________________________________________________________________________________________
    embedding_dayofweek (Embedding) (None, 1440, 2)      14          input_dayofweek[0][0]            
    __________________________________________________________________________________________________
    embedding_day (Embedding)       (None, 1440, 2)      62          input_day[0][0]                  
    __________________________________________________________________________________________________
    input_clock (InputLayer)        (None, 1440, 2)      0                                            
    __________________________________________________________________________________________________
    concatenate_1 (Concatenate)     (None, 1440, 24)     0           input_sensor[0][0]               
                                                                     input_common[0][0]               
                                                                     embedding_month[0][0]            
                                                                     embedding_dayofweek[0][0]        
                                                                     embedding_day[0][0]              
                                                                     input_clock[0][0]                
    __________________________________________________________________________________________________
    conv1d_1 (Conv1D)               (None, 1440, 64)     4672        concatenate_1[0][0]              
    __________________________________________________________________________________________________
    leaky_re_lu_1 (LeakyReLU)       (None, 1440, 64)     0           conv1d_1[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_1 (MaxPooling1D)  (None, 480, 64)      0           leaky_re_lu_1[0][0]              
    __________________________________________________________________________________________________
    conv1d_2 (Conv1D)               (None, 480, 96)      18528       max_pooling1d_1[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_2 (LeakyReLU)       (None, 480, 96)      0           conv1d_2[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_2 (MaxPooling1D)  (None, 240, 96)      0           leaky_re_lu_2[0][0]              
    __________________________________________________________________________________________________
    conv1d_3 (Conv1D)               (None, 240, 128)     36992       max_pooling1d_2[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_3 (LeakyReLU)       (None, 240, 128)     0           conv1d_3[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_3 (MaxPooling1D)  (None, 120, 128)     0           leaky_re_lu_3[0][0]              
    __________________________________________________________________________________________________
    conv1d_4 (Conv1D)               (None, 120, 192)     73920       max_pooling1d_3[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_4 (LeakyReLU)       (None, 120, 192)     0           conv1d_4[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_4 (MaxPooling1D)  (None, 60, 192)      0           leaky_re_lu_4[0][0]              
    __________________________________________________________________________________________________
    conv1d_5 (Conv1D)               (None, 60, 256)      147712      max_pooling1d_4[0][0]            
    __________________________________________________________________________________________________
    leaky_re_lu_5 (LeakyReLU)       (None, 60, 256)      0           conv1d_5[0][0]                   
    __________________________________________________________________________________________________
    max_pooling1d_5 (MaxPooling1D)  (None, 12, 256)      0           leaky_re_lu_5[0][0]              
    __________________________________________________________________________________________________
    flatten_1 (Flatten)             (None, 3072)         0           max_pooling1d_5[0][0]            
    __________________________________________________________________________________________________
    dense_4 (Dense)                 (None, 1)            3073        flatten_1[0][0]                  
    ==================================================================================================
    Total params: 284,997
    Trainable params: 3,073
    Non-trainable params: 281,924
    __________________________________________________________________________________________________
    Epoch 1/5
    3793/3793 [==============================] - 505s 133ms/step - loss: 0.0331 - acc: 0.9988
    Epoch 2/5
    3793/3793 [==============================] - 870s 229ms/step - loss: 0.0065 - acc: 0.9996
    Epoch 3/5
    3793/3793 [==============================] - 58s 15ms/step - loss: 0.0059 - acc: 0.9996
    Epoch 4/5
    3793/3793 [==============================] - 445s 117ms/step - loss: 0.0056 - acc: 0.9996
    Epoch 5/5
    3793/3793 [==============================] - 61s 16ms/step - loss: 0.0054 - acc: 0.9996
    




    <keras.callbacks.History at 0x7fd9c75b2080>




```python
predict = mm.predict_generator(rs)
```


```python
for _ in range(20, 30):
    s = slice(1440*_*4, 1440*(_+1)*4)
    plt.figure(figsize=(30,3))
    plt.plot(rs.MD[time_width-1:][s], predict[s], alpha=0.6)
    plt.figure(figsize=(30,3))
    plt.plot(rs.MD[time_width-1:][s], rs.MY[time_width-1:][s], alpha=0.6, c='r')
```


![png](output_135_0.png)



![png](output_135_1.png)



![png](output_135_2.png)



![png](output_135_3.png)



![png](output_135_4.png)



![png](output_135_5.png)



![png](output_135_6.png)



![png](output_135_7.png)



![png](output_135_8.png)



![png](output_135_9.png)



![png](output_135_10.png)



![png](output_135_11.png)



![png](output_135_12.png)



![png](output_135_13.png)



![png](output_135_14.png)



![png](output_135_15.png)



![png](output_135_16.png)



![png](output_135_17.png)



![png](output_135_18.png)



![png](output_135_19.png)

