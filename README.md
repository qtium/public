# public

This is a public repository by Tomohiro EGUCHI.

* [Sensor Data Package](commonlib/packages/homeiot)
* [MySQLdump file Import](commonlib/system/cosmos)
* [Neural Network Experiment](experiment/cnn_3label.md)
