"""Generate SQL used in import_mysqldump.sh
"""


def main():
    table_list = [
        "table_name_censored",
    ]

    for table in table_list:
        print(
            "CREATE TABLE IF NOT EXISTS schema_name_censored2.`{table}` LIKE schema_name_censored1.`{table}`;\n"
            "\! echo -n 'Processing {table} ... ';\n"
            "REPLACE INTO schema_name_censored2.`{table}` SELECT * FROM schema_name_censored1.`{table}`;\n"
            "\! echo 'Complete.'\n".format(table=table)
        )

if __name__ == '__main__':
    main()
