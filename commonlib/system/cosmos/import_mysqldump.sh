#!/bin/bash

usage() {
    cat <<__EOT__
Usage:
    $ bash import_mysqldump.sh [mysqldump file]...
FYI:
    If you would like to import all dump files,
    $ bash import_mysqldump.sh \`ls backup/*/db/*.sql\`
__EOT__
}


#------------------------------------------
#
# Purpose:
#   Copy records from the following
#   tables in mysqldump.
#
# MySQL schema:
#   schema_name_censored1 => schema_name_censored2
#
# Tables:
#   table_name_censored
#
#------------------------------------------


# START #
set -u

# Check mysql existence
if ! type mysql >/dev/null 2>&1; then
    echo 'mysql is not installed.'
    exit 1
fi

# Check python existence
if ! type python3 >/dev/null 2>&1; then
    echo 'python3 is not installed.'
    exit 1
fi

# Check args
if [ ${#} -lt 1 ]; then
    usage
    exit 1
fi


cd $(dirname ${BASH_SOURCE:-$0})


# PREPARE SQL FILE #
import_mysqldump_sql_filepath="_import_mysqldump.sql"
rm -f ${import_mysqldump_sql_filepath}
touch ${import_mysqldump_sql_filepath}

echo -n "Generating ${import_mysqldump_sql_filepath} ... "

echo "
-- Prepare schema to import
CREATE SCHEMA IF NOT EXISTS schema_name_censored1 CHARACTER SET = utf8;


-- Prepare schema as master
CREATE SCHEMA IF NOT EXISTS schema_name_censored2 CHARACTER SET = utf8;

" >> ${import_mysqldump_sql_filepath}


for i in `seq 1 ${#}`
do
    echo "
#-----------------------
# IMPORT ${1}
#-----------------------

-- Switch schema
USE schema_name_censored1;
SELECT schema();


-- Import mysqldump into schema_name_censored1
\! echo -n 'Importing ${1} ... '
SOURCE ${1};
\! echo 'Complete.'


-- Switch schema
USE schema_name_censored2;
SELECT schema();


-- Copy into master
\! echo 'Copy into master schema.'
`python3 import_mysqldump_gen.py 2>/dev/null`
\! echo 'All tables have been copied.'

" >> ${import_mysqldump_sql_filepath}

    shift
done

echo "Complete."


# EXECUTE SQL #
echo "Accessing mysql ..."
(
    mysql -p -u root -e "`cat ${import_mysqldump_sql_filepath}`" &&
    echo "Import completed."
) || echo "Import failed."

# FINISH #
