import numpy as np
import pandas as pd


class Preprocessor:
    def _add_column_on_off_action(self, df):
        """Remove keep-alive signal data.

        Judge by status(i) - status(i-1), which should be 0
        with keep-alive signal.

        e.g. [0, 0, 1, 0, 1, 1, 0, 0, 1] will be
             [0,    1, 0, 1,    0,    1]
        """
        for col, conf in self._config.items():
            if conf.get('on_off'):
                new_col = self._action_col(col)
                df.loc[:, new_col] = df.loc[:, col].diff()
        return df

    def _action_col(self, s):
         return '{}Action'.format(s)

    def _duration_col(self, s):
        return '{}Duration'.format(s)

    def _fix_datetime_col(self, df):
        df.loc[:, self._datetime_col] = df.loc[:, self._datetime_col].apply(lambda x: pd.Timestamp(x)).astype('<M8[ns]')
        return df

    @classmethod
    def mean(cls, col):
        return (lambda df, freq: df.loc[:, col]
                .resample(freq, closed='left', label='left')
                .mean()
                )
    @classmethod
    def count(cls, col, val):
        return (lambda df, freq: df.loc[:, col]
                .resample(freq, closed='left', label='left')
                .agg(lambda x: len(np.where(x.values == val)[0]))
                )
    @classmethod
    def sum(cls, col):
        return (lambda df, freq: df.loc[:, col]
                .resample(freq, closed='left', label='left')
                .apply(lambda x: np.sum(x.values))# sum is too slow!
                )
    @classmethod
    def interpolate(cls, col):
        return (lambda df, freq: df.loc[:, col]
                .interpolate(method='index')
                .asfreq(freq)
                )

    @classmethod
    def clock_dayofweek(cls, datetimeindex):
        phase = 2. * np.pi * datetimeindex.dayofweek / 7.
        return cls.circle(phase)

    @classmethod
    def clock_time(cls, datetimeindex):
        phase = 2. * np.pi * ((datetimeindex - datetimeindex.normalize()) / np.timedelta64(1, 'D'))
        return cls.circle(phase)

    @classmethod
    def circle(cls, phase):
        return np.array([np.cos(phase), np.sin(phase)]).T


class TimeSlicer(Preprocessor):
    """Class to reshape DataFrame with equally-sliced time index.
    """

    ON_OFF = 0
    ANALOG = 1

    def __init__(self, freq, config, datetime_col, range_):
        self._freq = freq
        self._config = config
        self._datetime_col = datetime_col
        self._range = range_

    def apply(self, df):
        df = df.copy(deep=True)
        df = self._add_column_on_off_action(df)
        df = self._fix_datetime_col(df)
        df = self._append_row_time_resample(df)
        df = self._add_column_on_duration(df)
        df = self._integrate(df)
        return df

    def _append_row_time_resample(self, df):
        """Insert exact time indices.

        getDatetime
        2018-03-01 00:14:29 <- index_time_recorded
        2018-03-01 00:15:00 <- index_time_resample
        2018-03-01 00:15:03 <- index_time_recorded
        """
        ## Avoid duplicate datetime from being index.
        dt_dup = df[self._datetime_col].duplicated()
        while len(df.loc[dt_dup]):
            df.loc[dt_dup, self._datetime_col] += pd.Timedelta(milliseconds=1)
            dt_dup = df[self._datetime_col].duplicated()
        ## Get new index
        index_time_recorded = df.loc[:, self._datetime_col]
        index_time_resample = pd.date_range(self._range[0], self._range[1],
                                            closed='left', freq=self._freq)
        index_merged = pd.Series(np.concatenate([index_time_recorded.values,
                                                 index_time_resample.values
                                                 ])
                                 ).unique()
        index_merged.sort()## This is np.ndarray!
        ## Set the merged time-series index.
        df = df.set_index(self._datetime_col, drop=True).reindex(index_merged, method=None)
        return df

    def _add_column_on_duration(self, df):
        """Calculate each record's duration.
        """
        for col, conf in self._config.items():
            if conf.get('on_off'):
                ## Calc each row duration.
                df.loc[:, conf['on_duration_name']] = df.index.to_series().diff().shift(-1)
                ## Fill the blank motion states of resample row.
                df.loc[:, col] = df.loc[:, col].fillna(method='ffill')
                ## Get duration of on state row.
                df.loc[df[col] == conf['off_state'], conf['on_duration_name']] = pd.Timedelta(0)
        return df

    def _integrate(self, df):
        """Resample by freq.
        """
        result = {}
        for col, conf in self._config.items():
            if conf.get('on_off'):
                result[conf['on_name']] = self.count(self._action_col(col), conf['on_state']-conf['off_state'])(df, self._freq)
                result[conf['off_name']] = self.count(self._action_col(col), conf['off_state']-conf['on_state'])(df, self._freq)
                result[conf['on_duration_name']] = self.sum(conf['on_duration_name'])(df, self._freq)
            if conf.get('analog'):
                result[conf['name']] = conf['aggr'](df, self._freq)
        df = pd.DataFrame(result)# concat is somewhat slow...
        return df
