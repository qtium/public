from .ill import *


class LPPConfig(ILLConfig):

    @classmethod
    def configure(cls, self):
        self.ILL_COL = 'col_censored'
        self.ILL_MAX = .2

        self.X_COL = [self.ILL_COL]

        self.FIGSIZE = {'w': 15, 'h': 2}

        self.time_slice_config = cls.time_slice_config(self)

class LPP(ILL):
    config = LPPConfig
