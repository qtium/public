from .ocs import *


class PND(OCS):

    def _load_df(self, kind_df, start, end):
        super()._load_df(kind_df, start, end)
        self._df = self._add_column_open_close(self._df)

    def _add_column_open_close(self, df):
        """Since PNA's table is so 'special', add openClose column.
        """
        def replace_status(x):
            if x == u'扉開':
                return 1
            if x == u'扉閉':
                return 0
            return None
        df.loc[:, 'openClose'] = df.loc[:, 'status'].apply(replace_status)
        df.dropna(subset=['openClose'], inplace=True)
        return df
