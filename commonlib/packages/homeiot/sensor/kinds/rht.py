from ..sensor import *


def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)


class RHTConfig:
    @classmethod
    def configure(cls, self):
        self.HUM_COL = 'humidity'
        self.TEMP_COL = 'temperature'

        self.X_COL = [self.HUM_COL, self.TEMP_COL]

        self.FIGSIZE = {'w': 15, 'h': 2}

        self.time_slice_config = cls.time_slice_config(self)

    @staticmethod
    def time_slice_config(self):
        config = {}
        config['humidity'] = {
            'name': 'humidity',
            'analog': True,
            'aggr': TimeSlicer.interpolate('humidity')
        }
        config['temperature'] = {
            'name': 'temperature',
            'analog': True,
            'aggr': TimeSlicer.interpolate('temperature')
        }
        if not self.wired:
            config['rssi'] = {
                'name': 'rssi',
                'analog': True,
                'aggr': TimeSlicer.mean('rssi')
            }
        return config


class RHT(Sensor):
    """Relative Humidity and Temperature Sensor class.
    """
    config = RHTConfig

    def _load_df(self, *args, **kwargs):
        super()._load_df(*args, **kwargs)
        self._df = self._df.rename(columns={'humibity': 'humidity'})# Fix typo in column name !!!!

    def _slice(self, freq):
        return RHTSliced(orig=self, freq=freq)


class RHTSliced(SensorSliced):

    def painter(self, head, tail):
        def layer(bbox=[0., 0., 1., 1.]):
            ## INIT -------------------------------------------------------------
            x = pd.date_range(head, tail, freq=self._freq, closed=None)
            range_ = self._df.copy(deep=True).reindex(x)
            fig = plt.gcf()

            ## TEMPERATURE ------------------------------------------------------
            ax = Visualizer.get_ax(bbox, [.15, .3, .75, .6])
            self._titlize(ax)
            ## xaxis
            ax.set_xlim(head, tail)
            ## yaxis
            ax.set_ylabel('temperature')
            ax.set_ylim(0., 50.)
            ax.yaxis.set_ticks(np.linspace(0., 50., 6))
            ## Plot
            lns = ax.plot(x, range_[self.TEMP_COL], alpha=0.5, color='firebrick')

            ## HUMIDITY ---------------------------------------------------------
            ax2 = ax.twinx()
            ## Draw axis at left. See the following url.
            ## https://stackoverflow.com/questions/20146652/two-y-axis-on-the-left-side-of-the-figure
            ax2.spines['left'].set_position(('outward', 50))
            make_patch_spines_invisible(ax2)
            ax2.spines['left'].set_visible(True)
            ax2.yaxis.set_label_position('left')
            ax2.yaxis.set_ticks_position('left')
            ## xaxis
            ax2.xaxis.set_ticks([])
            ## yaxis
            ax2.set_ylabel('humidity')
            ax2.set_ylim(0., 100.)
            ax2.yaxis.set_ticks(np.linspace(0., 100., 6))
            ## Plot
            lns = ax2.plot(x, range_[self.HUM_COL], alpha=0.5) + lns

            ## Legend
            ## https://stackoverflow.com/questions/5484922/secondary-axis-with-twinx-how-to-add-to-legend
            labs = [l.get_label() for l in lns]
            ax.legend(lns, labs, loc='lower right', ncol=2)

            ## RSSI -------------------------------------------------------------
            if not self.wired:
                self._plot_rssi(ax=ax.twinx(), x=x, range_=range_)

            Visualizer.dateformat_x(ax, '%m-%d(%a) %H:%M')
        return {'layer': [layer], 'size': self.FIGSIZE}

    def normalize(self):
        df = self.df.copy(deep=True)
        df.loc[:, self.HUM_COL] = df.loc[:, self.HUM_COL] / 100.
        df.loc[:, self.TEMP_COL] = (df.loc[:, self.TEMP_COL] - 20.) / 20.
        df.iloc[0] = df.iloc[1]# fillna
        return df
