import numpy as np
import pandas as pd
from ..sensor import *


class ILLConfig:

    @classmethod
    def configure(cls, self):
        self.ILL_COL = 'illuminance'
        self.ILL_MAX = 100.

        self.X_COL = [self.ILL_COL]

        self.FIGSIZE = {'w': 15, 'h': 2}

        self.time_slice_config = cls.time_slice_config(self)

    @staticmethod
    def time_slice_config(self):
        config = {}
        config[self.ILL_COL] = {
            'name': self.ILL_COL,
            'analog': True,
            'aggr': TimeSlicer.interpolate(self.ILL_COL)
        }
        if not self.wired:
            config['rssi'] = {
                'name': 'rssi',
                'analog': True,
                'aggr': TimeSlicer.mean('rssi')
            }
        return config


class ILL(Sensor):
    """Illuminance Sensor class.
    """
    config = ILLConfig

    def _slice(self, freq):
        return ILLSliced(orig=self, freq=freq)


class ILLSliced(SensorSliced):

    def painter(self, head, tail):
        def layer(bbox=[0., 0., 1., 1.]):
            ## INIT -------------------------------------------------------------
            x = pd.date_range(head, tail, freq=self._freq, closed=None)
            range_ = self._df.copy(deep=True).reindex(x)
            fig = plt.gcf()
            ## ILLMINANCE -------------------------------------------------------
            ax = Visualizer.get_ax(bbox, [.15, .3, .75, .6])
            self._titlize(ax)
            max_ = max(self.ILL_MAX, range_[self.ILL_COL].max() * 1.05)
            ## xaxis
            ax.set_xlim(head, tail)
            ## yaxis
            ax.set_ylabel('illuminance')
            ax.set_ylim(0., max_)
            ax.yaxis.set_ticks(np.linspace(0., max_, 6))
            ## Plot
            ax.plot(x, range_[self.ILL_COL], alpha=0.5)

            ## RSSI -------------------------------------------------------------
            if not self.wired:
                self._plot_rssi(ax=ax.twinx(), x=x, range_=range_)

            Visualizer.dateformat_x(ax, '%m-%d(%a) %H:%M')

        return {'layer': [layer], 'size': self.FIGSIZE}


    def normalize(self):
        df = self.df.copy(deep=True)
        df.loc[:, self.ILL_COL] = np.log(1. + df.loc[:, self.ILL_COL]) / 7.
        df.iloc[0] = df.iloc[1]# fillna
        return df
