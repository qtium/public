from ..sensor import *


class PIRConfig:
    @classmethod
    def configure(cls, self):
        self.ON_STATE = 1
        self.OFF_STATE = 0
        self.ON_ACTION = 1
        self.OFF_ACTION = -1

        self.ON_OFF_COL = 'col_censored'
        self.ON_DURATION = 'col_censored'
        self.ON_COL = 'getIn'
        self.OFF_COL = 'getOut'

        self.X_COL = [self.ON_COL,
                  self.OFF_COL,
                  self.ON_DURATION
                  ]

        self.FIGSIZE = {'w': 15, 'h': 3}

        self.time_slice_config = cls.time_slice_config(self)

    @staticmethod
    def time_slice_config(self):
        config = {}
        config['motion'] = {
            'name': 'motion',
            'on_off': True,
            'on_state': 1,
            'off_state': 0,
            'on_name': 'getIn',
            'off_name': 'getOut',
            'on_duration_name': 'inDuration'
        }
        if not self.wired:
            config['rssi'] = {
                'name': 'rssi',
                'analog': True,
                'aggr': TimeSlicer.mean('rssi')
            }
        return config


class PIR(Sensor):
    """Passive Infra-Red class.(Person IN the Room...?)
    """
    config = PIRConfig

    def _slice(self, freq):
        return PIRSliced(orig=self, freq=freq)


class PIRSliced(SensorSliced):

    def painter(self, head, tail):
        def layer(bbox=[0., 0., 1., 1.]):
            self._viz_on_off(head, tail, bbox)
        return {'layer': [layer], 'size': self.FIGSIZE}

    def normalize(self):
        df = self.df.copy(deep=True)
        df.loc[:, self.ON_DURATION] = df.loc[:, self.ON_DURATION].astype("timedelta64[s]").astype("float") / 60.
        return df
