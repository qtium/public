import datetime
from homeiot.sensor.config import (
    get_connection,
    DF_PROC_ON_SQL,
    PATH_DBLOG,
    filepath_cache_csv,
    filepath_cache_pickle,
    )
import os
import pandas as pd
import pandas.tseries.offsets as offsets
from pathlib import Path
import pdutil.unique

from logging import getLogger, StreamHandler, INFO
logger = getLogger(__name__)
handler = StreamHandler()
handler.setLevel(INFO)
logger.setLevel(INFO)
logger.addHandler(handler)


def load_kind_df(meta, start, end):
    """
    Yields:
        pd.DataFrame for each unique table in meta.

    Explain:
         05/01                                             07/01
        [head_date -------- [Range SQL fetch] ---------- tail_date)
            [start -------- [Range you want] ------ end)
             05/05                                 06/15
        Cache (csv and pkl) will be generated for 5th and 6th month.
    """
    tables = pdutil.unique(pd.DataFrame(meta), "table",
                           select_columns=["table", "datetime_col", "kind"])
    start, end = pd.Timestamp(start), pd.Timestamp(end)
    head_date = start.replace(day=1)
    ## -1 seconds for prevent including right.
    tail_date = end - offsets.Second() + offsets.MonthBegin(normalize=True)
    for table_meta in tables.itertuples(index=False):
        table, datetime_col, kind = table_meta
        ## Iterate for each table.
        try:
            df = _get_cache(table_meta, head_date, tail_date)
        except CacheNotFoundException:
            logger.info("DataFrame cache is not found. Loading from DB.")
            logger.info("[TABLE] {}".format(table))
            df = _sql_to_df("SELECT * "
                            "FROM `{table}` "
                            "WHERE "
                            "{datetime_col} BETWEEN %(left)s AND %(right)s "
                            "ORDER BY {datetime_col} ASC".format(table=table,
                                                                 datetime_col=datetime_col),
                            table_meta,
                            left=head_date.strftime('%Y%m%d'), right=tail_date.strftime('%Y%m%d')
                            )
            _set_cache(df, table_meta, head_date, tail_date)
        yield (kind,
               df.query("'{left}' <= {datetime_col} < '{right}'".format(
                        datetime_col=datetime_col,
                        left=start, right=end)))


def _sql_to_df(sql, table_meta, **params):
    """Query sql to schema_name_censored2.
    """
    with get_connection() as con:
        df = pd.read_sql(sql, con=con, params=params)
    _apply_df_proc_on_sql(df, table_meta.table)
    return df


def _apply_df_proc_on_sql(df, table):
    for col, func in DF_PROC_ON_SQL.get(table, {}).items():
        df.loc[:, col] = df.loc[:, col].map(func)


class CacheNotFoundException(Exception):
    pass


def _get_cache(table_meta, head_date, tail_date):
    """Try to get data from pickle.

    Even if only a single file is missing, it is
    treated as fault.

    Args:
        table: Table name.
        head_date: Represented like '%Y%m01'.
        tail_date: Represented like '%Y%m01'.
            See load_df in detail.
    """
    ## Check if all files exist.
    if __all_cache_files_exist(table_meta.table, head_date, tail_date):
        return pd.concat([pd.read_pickle(filepath_cache_pickle(month, table_meta.table))
                         for month in pd.date_range(head_date, tail_date, freq='M')], axis=0)
    else:
        raise CacheNotFoundException()


def __all_cache_files_exist(table, head_date, tail_date):
    return all([Path(filepath_cache_pickle(month, table)).is_file()
               for month in pd.date_range(head_date, tail_date, freq='M')])


def _set_cache(df, table_meta, head_date, tail_date):
    """Make pd.DataFrame output monthly csv and pickle.

    Args:
        df (pd.DataFrame): Output of _sql_to_df.
        table: Table name.
        head_date: Represented like '%Y%m01'.
        tail_date: Represented like '%Y%m01'.
            See load_df in detail.
    """
    for month in pd.date_range(head_date, tail_date, freq='MS', closed='left'):
        ## month is actually end of month
        month_head_date = month
        month_tail_date = month_head_date + offsets.MonthBegin()
        ## Extract data within the month.
        monthly_df = df.query("{left} <= {datetime_col} < {right}".format(
                              datetime_col=table_meta.datetime_col,
                              left=month_head_date.strftime('%Y%m%d'),
                              right=month_tail_date.strftime('%Y%m%d')))
        ## Output csv. Just for human-readable ...
        os.makedirs(PATH_DBLOG / "{Ym}/csv".format(Ym=month.strftime('%Y%m')), exist_ok=True)
        monthly_df.to_csv(filepath_cache_csv(month, table_meta.table), index=False)
        ## Output pickle.
        os.makedirs(PATH_DBLOG / "{Ym}/pkl".format(Ym=month.strftime('%Y%m')), exist_ok=True)
        monthly_df.to_pickle(filepath_cache_pickle(month, table_meta.table))

