
class SensorMetaBase:
    def __str__(self):
        return self._meta.__str__()

    def __repr__(self):
        return self._meta.__repr__()

    def _repr_html_(self):## For Jupyter
        try:
            return self._meta._repr_html_()
        except AttributeError:
            return self._meta.to_frame()._repr_html_()

    @property
    def meta(self):
        return self._meta

    @property
    def range(self):
        return self._range

    @property
    def start(self):
        return self._range[0]

    @property
    def end(self):
        return self._range[1]


def sensor_class_of(kind):
    """Dynamically returns sensor instance with respect to kind.
    """
    import homeiot
    import importlib
    try:
        ## Dynamic import
        m = importlib.import_module('homeiot.sensor.kinds.{}'.format(kind.lower()))
        return getattr(m, kind)
    except (ModuleNotFoundError, AttributeError):
        return homeiot.Sensor
