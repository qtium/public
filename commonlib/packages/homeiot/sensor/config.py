import pandas as pd
from pathlib import Path
import pdutil.unique
from homeiot.config import PATH_ROOT


"""Define path list used in sensor module.
"""
PATH_DBLOG = PATH_ROOT / "source_data" / "sensor" / "dblog"
PATH_SENSOR = PATH_ROOT / "packages" / "homeiot" / "sensor"


def filepath_cache_csv(month, table):
    return PATH_DBLOG / "{Ym}/csv/{table}.csv".format(Ym=month.strftime('%Y%m'), table=table)


def filepath_cache_pickle(month, table):
    return PATH_DBLOG / "{Ym}/pkl/{table}.pkl".format(Ym=month.strftime('%Y%m'), table=table)


"""Define column-wise processing after query
"""
def decode(charset):
    return lambda s: s.decode(charset) if isinstance(s, bytearray) else s

utf8_decoder = decode('utf-8')


DF_PROC_ON_SQL = {
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "historyCategory": utf8_decoder,
        "operationSource": utf8_decoder,
        "operationTarget": utf8_decoder,
        "Contents": utf8_decoder,
        "status": utf8_decoder,
        "detail": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    },
    "table_name_censored": {
        "sensorId": utf8_decoder
    }
}



"""Define connection info.
"""
_DB_SETTING = {
    "host": "0.0.0.0",
    "port": "0",
    "user": "user_censored",
    "password": "password_censored",
    "charset": "utf8",
    "database": "schema_name_censored",
}


class MySQLConnectionWrapper:
    def __init__(self, **kwargs):
        self.kwargs = kwargs

    def __enter__(self):
        import mysql.connector as db
        self.con = db.connect(**self.kwargs)
        return self.con

    def __exit__(self, exception_type, exception_value, traceback):
        return self.con.close()


def get_connection():
    """Establish connection to MySQL server.
    """
    return MySQLConnectionWrapper(**_DB_SETTING)


def get_meta(meta=None, unique=False, **kwargs):
    """Get sensor meta information with reference to the csv file.
    See "sensor_list.csv".
    """
    if meta is None:
        ## Try cache
        if hasattr(get_meta, "sensor_meta"):
            sensor_meta = get_meta.sensor_meta
        else:
            ## Cache fail.
            sensor_meta = pd.read_csv(PATH_SENSOR / 'sensor_list.csv',
                                    dtype={'room': str}
                                    )
            get_meta.sensor_meta = sensor_meta
    else:# Use given limited meta.
        sensor_meta = meta

    ## Filter by kwargs
    if len(kwargs):
        _cast_value_as_list(kwargs)
        if 'place' in kwargs:
            # String matching (like)
            place = kwargs.pop('place')
            contain = (sensor_meta.loc[:, 'place']
                        .apply(lambda x: any([s in x.split(" ") for s in place]))
                        )
            sensor_meta = sensor_meta.loc[contain, :]
        if len(kwargs):# When ohters remain
            val = sensor_meta.loc[:, kwargs.keys()]
            sensor_meta = sensor_meta.loc[val.isin(kwargs).all(axis=1), :]

    ## SELECT DISTINCT
    if unique:
        sensor_meta = pdutil.unique(sensor_meta, unique)
    return sensor_meta


def _cast_value_as_list(dict_):
    """Ensure values in dict as list.
    Examples
    --------
    >>> _cast_value_as_list({"a": 1, "b": [1]})
    {'a': [1], 'b': [1]}
    """
    for k, v in dict_.items():
        dict_[k] = v if isinstance(v, list) else [v]
    return dict_
