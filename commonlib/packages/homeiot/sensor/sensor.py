from homeiot.sensor.sensor_meta_base import SensorMetaBase
from homeiot.sensor.sensor_net import SensorNet
from homeiot.preprocessor import TimeSlicer
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from homeiot import Visualizer


"""Exception
"""
class SensorMethodNotImplementedError(NotImplementedError):
    pass


class SensorNotSlicedError(Exception):
    pass


class SensorDataFrameNotLoadedException(Exception):
    pass


class SensorBase(SensorMetaBase):
    def __getattr__(self, attr):
        """Enable access to each meta column like an attribute.
        """
        try:
            return self.meta[attr]
        except KeyError:
            return super().__getattribute__(attr)

    def __getitem__(self, item):
        """Enable access to each meta column like list object.
        """
        return self.meta[item]

    @property
    def df(self):
        return self._df

    @property
    def where(self):
        return "{}{}".format(self.room, self.place.split(' ')[0])


class Sensor(SensorBase):
    """Supposed to be generated from SensorNet.
    """
    def __init__(self, meta, *args, **kwargs):
        """
        Args:
            meta: pd.Series
        """
        self._meta = meta
        self._df = None
        self._range = (None, None)# start, end
        self._sliced = {}

    def _load_df(self, kind_df, start, end):
        """Load DataFrame for each sensor.

        Args:
            kind_df (pd.DataFrame):
                This table DataFrame will be used to load sensor
                DataFrame if supplied.
        """
        self._df = kind_df.loc[kind_df.loc[:, self.id_col] == self.id_val].copy(deep=True)
        self._range = (start, end)
        self._sliced = {}

    """ Time Slice ----------------------------------------------------------------- """
    def slice(self, freq='1min'):
        """Slice DataFrame by freq.

        args:
            freq (str):
                '15min', 'min', '30s', ...
        """
        if self._df is None:
            raise SensorDataFrameNotLoadedException()
        if freq not in self._sliced:
            self._sliced[freq] = self._slice(freq)
        return self._sliced[freq]

    def _slice(self, freq):
        raise SensorMethodNotImplementedError()

    @property
    def slice_list(self):
        return self._sliced.keys()


class SensorProcessed(SensorBase):
    def __init__(self, orig):
        self._meta = orig.meta
        self._df = orig.df.copy(deep=True)
        self._range = orig.range
        self._orig = orig


class SensorSliced(SensorProcessed):
    def __init__(self, orig, freq):
        super().__init__(orig)
        self._freq = freq
        orig.config.configure(self)
        ## slice
        self._df = TimeSlicer(freq=freq, config=self.time_slice_config,
                              datetime_col=self.datetime_col, range_=self._range
                              ).apply(self._df)

    ## Visualization -------------------------------------------------------##
    def viz(self, *args, **kwargs):
        SensorNet(sensor=self._orig).slice(freq=self._freq).viz(*args, **kwargs)

    def painter(self, head, tail):
        """Return method object.
        """
        raise SensorMethodNotImplementedError()

    def _viz_on_off(self, head, tail, bbox=[0., 0., 1., 1.]):
        ## INIT -----------------------------------------------------------------
        x = pd.date_range(head, tail, freq=self._freq, closed=None)
        range_ = self._df.copy(deep=True).reindex(x)
        fig = plt.gcf()

        ## ON DURATION ----------------------------------------------------------
        ax = Visualizer.get_ax(bbox, [.15, .5, .75, .4])
        self._titlize(ax)
        ## xaxis
        ax.set_xlim(head, tail)
        ax.tick_params(labelbottom=False)
        ## yaxis
        ax.set_ylim(0., 1.)
        ax.yaxis.set_ticks(np.linspace(0., 1., 6))
        ax.set_ylabel('{} ratio\n\n'.format(self.ON_DURATION),
                      rotation=90, ha='center', va='center')
        ## Plot
        y = range_[self.ON_DURATION].values / pd.to_timedelta(self._freq)
        ax.fill_between(x, y, alpha=.5, lw=0., step='post')

        ## RSSI -----------------------------------------------------------------
        if not self.wired:
            self._plot_rssi(ax=ax.twinx(), x=x, range_=range_)

        ## ON ACTION ------------------------------------------------------------
        on_ax = Visualizer.get_ax(bbox, [.15, .42, .75, .08])
        ## xaxis
        on_ax.set_xlim(head, tail)
        on_ax.tick_params(labelbottom=False)
        ## yaxis
        on_ax.set_ylabel(self.ON_COL, rotation=0, ha='right', va='top')
        on_ax.set_ylim(0, 1)
        on_ax.yaxis.set_ticks([])
        ## Plot
        y = range_[self.ON_COL].map(lambda e: 1 if e > 0 else 0)
        on_ax.fill_between(x, y, alpha=.5, lw=0., step='post', color='seagreen')

        ## OFF ACTION -----------------------------------------------------------
        off_ax = Visualizer.get_ax(bbox, [.15, .34, .75, .08])
        ## xaxis
        off_ax.set_xlim(head, tail)
        Visualizer.dateformat_x(off_ax, '%m-%d(%a) %H:%M')
        ## yaxis
        off_ax.set_ylabel(self.OFF_COL, rotation=0, ha='right', va='top')
        off_ax.set_ylim(0, 1)
        off_ax.yaxis.set_ticks([])
        ## Plot
        y = range_[self.OFF_COL].map(lambda e: 1 if e > 0 else 0)
        off_ax.fill_between(x, y, alpha=.5, lw=0., step='post', color='firebrick')

    def _titlize(self, ax):
        ax.set_title("Room{} - {} - {} - {}".format(self.room, self.id, self.place, self.m2m_id))

    def _plot_rssi(self, ax, x, range_):
        ## yaxis
        ax.set_ylim(-100., -40.)
        ax.yaxis.set_ticks(np.linspace(-100., -40., 6))
        ax.set_ylabel('rssi [dBm]')
        ## Plot
        ax.scatter(x, range_['rssi'].values, marker='*', s=24, alpha=.4, color='darkorange')

    def getX(self, index=None):
        df = self.normalize()
        if index is None:
            index = df.index
        return df.loc[index, self.X_COL].values
