from homeiot.sensor.config import get_meta
from homeiot.sensor.dataloader import load_kind_df
from homeiot.sensor.sensor_meta_base import sensor_class_of
from homeiot.sensor.sensor_meta_base import SensorMetaBase
from homeiot import Visualizer
import matplotlib.cm as cm
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import pdutil
from sklearn.preprocessing import LabelEncoder


class NoSensorFoundException(Exception):
    pass


class ForbiddenDataFrameLoad(Exception):
    pass


class SensorNetBase(SensorMetaBase):
    """Represent multiple (or single) sensor network.
    """

    def __init__(self, orig=None, **cond):
        """Select sensors specified by cond.

        Args:
            cond:
                Header in sensor_list.csv can be used.
                id, category, kind, wired, tag, table, id_col, id_val, datetime_col, m2m_id, room, place, serial_id

        Examples:
            >>> sensor_net = homeiot.SensorNet(room="101", kind="OCS", id=["OCS007", "OCS012"])
            >>> sensor_net
                    id category kind  wired             tag    ...    datetime_col  m2m_id room          place serial_id
            78      censored
            83      censored

            [2 rows x 13 columns]
        """
        if orig is None:
            self._meta = get_meta(**cond)
            self._range = (None, None)# start, end
            self._create_sensors()
            self._is_sub = False
        else:
            self._meta = get_meta(meta=orig.meta, **cond)
            self._range = orig.range
            self._sensor_dict = {id_: orig._sensor_dict[id_] for id_ in self._meta.id.values}
            self._is_sub = True
            self.orig = orig

        if len(self._meta) == 0:
            raise NoSensorFoundException()

    def _create_sensors(self):
        _iloc = self._meta.iloc
        self._sensor_dict = {_iloc[idx].at['id']: sensor_class_of(_iloc[idx].at['kind'])(meta=_iloc[idx])
                             for idx in range(self._meta.shape[0])
                             }

    def __getattr__(self, attr):
        """Enable access to each sensor like an attribute.

        Examples:
            >>> sensor_net.OCS007
            id                              OCS007
            category                           OCS
            kind                               OCS
            wired                            False
            id_col                        sensorId
            id_val                          OCS007
            datetime_col               getDatetime
            m2m_id                          M2M002
            room                               101
            place                        room door
            Name: 78, dtype: object
        """
        try:
            return self._sensor_dict[attr]
        except KeyError:
            return super().__getattribute__(attr)

    def __getitem__(self, item):
        """Enable access to each sensor like list object.

        >>> sensor_net["OCS007"]
        id                              OCS007
        category                           OCS
        kind                               OCS
        wired                            False
        id_col                        sensorId
        id_val                          OCS007
        datetime_col               getDatetime
        m2m_id                          M2M002
        room                               101
        place                        room door
        Name: 78, dtype: object
        """
        return self._sensor_dict[item]

    def __iter__(self):
        """Enable "for" clause on sensors.
        """
        for s in self._sensor_dict.values():
            yield s

    def __len__(self):
        return len(self._sensor_dict)

    def subnet(self, **cond):
        """Extract sub-SensorNet with respect to given cond.

        Be careful that sensor instances in sub-SensorNet is shallow-copied.
        """
        sub_sn = self.__class__(orig=self, **cond)
        return sub_sn

    def drop(self, **cond):
        target = get_meta(meta=self._meta, **cond)
        for id_ in target.id:
            del self._sensor_dict[id_]
        self._meta = self._meta.drop(index=target.index)
        return self

class SensorNet(SensorNetBase):
    def __init__(self, sensor=None, *args, **kwargs):
        if sensor is not None:
            self._meta = sensor.meta.to_frame().T
            self._sensor_dict = {sensor.id: sensor}
            self._is_sub = True
            self._range = sensor.range
        else:
            super().__init__(*args, **kwargs)

    def load_df(self, start, end):
        """Load DataFrame for each sensor.

        Args:
            start, end:
                Data within [start, end) will be loaded.
                So, start is included, and end is NOT.
        """
        if self._is_sub:
            raise ForbiddenDataFrameLoad()
        df_dict = {kind: kind_df for kind, kind_df
                         in load_kind_df(self.meta, start, end)
                         }
        for s in self:
            s._load_df(kind_df=df_dict[s.kind],
                       start=start,
                       end=end
                       )
        self._range = (start, end)
        return self

    def slice(self, freq='1min'):
        """Return SensorNet which holds time sliced DataFrame.
        """
        sliced = SensorNetSliced(orig=self, freq=freq)
        return sliced

    @property
    def slice_list(self):
        return {s.id: s.slice_list for s in self}


class SensorNetSliced(SensorNetBase):
    def __init__(self, freq=None, *args, **kwargs):
        """ Always _is_sub True
        """
        super().__init__(*args, **kwargs)
        if freq is None:
            self._freq = self.orig.freq
        else:
            self._freq = freq
            self._sensor_dict = {s.id: s.slice(freq) for s in self}
        self._sensor_viz_order = sorted(self, key=lambda s: s.room + s.place)
        self.cmap = cm.get_cmap('tab10')
        self._init_where_encoder()

    @property
    def freq(self):
        return self._freq

    def viz(self, freq='D', start=None, end=None, path=None, one_file=False):
        visualizer = Visualizer(path=path, one_file=one_file, filenamer=self.filename)

        ## Check input.
        if start is None:
            start = self.start
        if end is None:
            end = self.end

        for head, tail in pdutil.interval_range(start, end, freq):
            self.entrustViz(visualizer, head, tail)
            visualizer.paint(filename_args=(path, head, tail))
        visualizer.finalize()
        return self

    def entrustViz(self, visualizer, head, tail):
        for s in self._sensor_viz_order:
            visualizer.append(s.painter(head, tail))
            color = self.cmap(self._label_encoder.transform([s.where])[0])
            visualizer.overlay(self.layer_background(color))# Change background color by sensor place.

    def filename(self, path, head, tail):
        path = Path(path)
        return (str(path.parent / head.strftime("[%Y%m%d%H%M]"))
                                + tail.strftime("[%Y%m%d%H%M]")
                                + path.name
                )

    def layer_background(self, color):
        """Set color in accordance with place.

        This assumes that 1st word of place keyword is decisive.
        """
        def layer(bbox):
            fig = plt.gcf()
            rect = Rectangle((bbox[0], bbox[1]), width=bbox[2], height=bbox[3], zorder=-1,
                            alpha=.15, edgecolor=None, facecolor=color,
                            transform=fig.transFigure)
            fig.patches.append(rect)
        return {'layer': [layer]}

    def _init_where_encoder(self):
        label_list = []
        for s in self:
            label_list.append(s.where)
        label_list = list(tuple(label_list))
        self._label_encoder = LabelEncoder().fit(label_list)
        return self._label_encoder

    def getX(self, index=None):
        X = []
        for s in self:
            X.append(
                s.getX(index=index)
            )
        return np.concatenate(X, axis=1)
