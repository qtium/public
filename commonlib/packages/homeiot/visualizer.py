from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.dates as mdates
from matplotlib.font_manager import FontProperties
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns


class Visualizer:
    """Visualizer with pyplot and seaborn
    """
    def __init__(self, path=None, one_file=False, filenamer=None):
        """
        Augs:
            path: Where to save the figure.
            one_file: If the path extension is .pdf file, save in a single file.
            filenamer: Function deciding filenames in the iteration.
        """
        self.path = path
        self.one_file = one_file

        ## One file pdf module
        self.pdf = None
        if path is not None:
            if path.split('.')[-1] == 'pdf' and one_file:
                self.pdf = PdfPages(path)

        ## Avoid overwriting multiple file into one path
        self.filenamer = filenamer if callable(filenamer) else lambda: self.path
        self._clear_painter()
        self._set_plt_params()

    def _set_plt_params(self):
        """seaborn settings
        """
        sns.set()
        sns.set_style('whitegrid', {'grid.linestyle': '--'})
        font_path = '/usr/share/fonts/truetype/takao-gothic/TakaoPGothic.ttf'
        font_prop = FontProperties(fname=font_path)
        plt.rcParams['font.family'] = font_prop.get_name()
        plt.rcParams['font.size'] = 10
        plt.rcParams['axes.titlesize'] = 11

    """Manage painter
    """
    def _clear_painter(self):
        self.painter_list = []

    def append(self, painter):
        """
        Args:
            painter: callable with arg 'bbox'
        """
        self.painter_list.append(painter)

    def overlay(self, painter):
        self.painter_list[-1].get('layer').extend(painter.get('layer'))

    def _flush(self, filename_args):
        if self.path is not None:
            if self.pdf is not None:
                self.pdf.savefig()
            else:
                plt.savefig(self.filenamer(filename_args))
        else:
            plt.show()

    def paint(self, filename_args=()):
        """Execute painters in painter_list
        Args:
            filename_args: This will be supplied to filenamer.
        """
        figsize = self._vstack_size([painter.get('size') for painter in self.painter_list])
        fig = plt.figure(figsize=(figsize['w'], figsize['h']))
        for painter, bbox in zip(self.painter_list, figsize['bbox']):
            for layer in painter.get('layer'):
                layer(bbox)
        self._flush(filename_args)
        self._clear_painter()
        fig.clf()

    def finalize(self):
        if self.pdf is not None:
            self.pdf.close()

    def _vstack_size(self, size_list):
        """Calculate graph position of vertically stacked graphs.
        Origin point is lower left.
        """
        w = max([size.get('w') for size in size_list])
        h_list = [size.get('h') for size in size_list]
        cum = np.cumsum(h_list)
        h = cum[-1]
        cum = h - cum
        bbox = [[0., c_/h, 1., h_/h] for c_, h_ in zip(cum, h_list)]
        return {"w": w, "h": h, "bbox": bbox}

    @staticmethod
    def get_ax(bbox1, bbox2):
        """bbox2 inside bbox1
        """
        return plt.gcf().add_axes(Visualizer.bbox_mul(bbox1, bbox2))

    @staticmethod
    def bbox_mul(outer, inner):
        """Calculate inner bbox position
        """
        inner[0] = inner[0] * outer[2] + outer[0]
        inner[1] = inner[1] * outer[3] + outer[1]
        inner[2] = inner[2] * outer[2]
        inner[3] = inner[3] * outer[3]
        return inner

    @staticmethod
    def dateformat_x(ax, format_):
        ax.xaxis.set_major_locator(mdates.AutoDateLocator())
        ax.xaxis.set_major_formatter(mdates.DateFormatter(format_))
        plt.setp(ax.get_xticklabels(), rotation=15, ha='right')# date xlabel
