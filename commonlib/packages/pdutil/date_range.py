import pandas as pd
from pandas.tseries.frequencies import to_offset


def date_range(start, end, freq):
    """
           [   ][   ][   ][   ][   ][   ][   ] <- pd.date_range returns.
        start --------------------------- end
      [   ][   ][   ][   ][   ][   ][   ][   ] <- This returns.
    """
    range_ = pd.date_range(start, end, freq=freq, closed='left')
    if pd.Timestamp(start) != range_[0]:
        head = range_[0] - to_offset(freq)
        range_ = [head] + list(range_)
    return pd.DatetimeIndex(range_, freq=freq)


def interval_range(start, end, freq):
    """
    Be careful to use 'MS' for month-start.
    """
    range_ = pd.date_range(start, end, freq=freq, closed='left')
    if len(range_) == 0:
        range_ = pd.date_range(pd.Timestamp(start) - to_offset(freq), end, freq=freq, closed='left')
    elif pd.Timestamp(start) != range_[0]:
        head = range_[0] - to_offset(freq)
        range_ = [head] + list(range_)
    for r in range_:
        yield r, r + to_offset(freq)
