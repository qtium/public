
def unique(df, columns, select_columns=None):
    uniq = ~df.loc[:, columns].duplicated()
    if select_columns is None:
        return df.loc[uniq, :]
    else:
        return df.loc[uniq, select_columns]
